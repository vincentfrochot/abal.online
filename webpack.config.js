const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack'); //to access built-in plugins

module.exports = (env) => {
  return {
    // webpack will take the files from ./src/index
    entry: './src/index.jsx',

    // and output it into /dist as bundle.js
    output: {
      path: path.join(__dirname, '/dist'),
      filename: 'bundle.js',
    },

    resolve: {
      extensions: ['.jsx', '.js'],
    },

    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      port: 8080,
    },

    module: {
      rules: [
        { test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
        { test: /\.(js)x?$/, exclude: /node_modules/, loader: 'babel-loader' },
        { // fixing ERR_UNKNOWN_URL_SCHEME in local
          test: /\.js$/,
          enforce: 'pre',
          use: ['source-map-loader'],
        },
        {
          test: /\.(mp3|ogg|flac|png|jpe?g|gif)$/i,
          loader: 'file-loader',
        },
      ],
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: 'public/index.html',
        filename: 'index.html',
      }),
      new webpack.DefinePlugin({
        'process.env.API_URL': JSON.stringify(`${env.API_URL}`),
      }),
      new webpack.DefinePlugin({
        'process.env.SOCKET_URL': JSON.stringify(`${env.SOCKET_URL}`),
      }),
    ],
  };
};
