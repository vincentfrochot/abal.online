"setup" 19-20
OK          turn based game
OK          score
OK          arrows
OK          all moves
OK          "secure" replay mode (can't go too high or too low)
        20
OK          marble "turn indicator"
OK          end the game when 6th marble was pushed out                 
OK          display message when you win or lose
        20-21
OK          sockets system in http
OK          show list of players
        25
            handle time correctly
            give 1 color to each player in online game (black player can't select white marbles)
        26
            Setup new architecture for the new website : abal.online/v2/
            sign up, sign in
            display "online game" or "local game" on a game
            display players nicknames if it is online
            display list of watchers as Chips
            list online games
            create online game instead of local one (and set timers)
            do not display timers if it is not an online game
        27 : Musichess tournament => online play using timers + accounts + save games in database



        JULY : focus on spectator & analysis tools

            compute moves list in PEARL system
            make a "draw" if both players played 2 times in a row the same back and forth
            be able to load a position from URL (also add the possibility to say which turn it is, so for example the situation can be started at white turn)

            ,
            ask to cancel a move (opponent must accept)
            timers (poc) socketIO will send the timers updated values after each move.
            implement server-side moves verification
        22	rejoin game to play or watch, write players names, write watchers names in chips
(June)  23	ask to cancel a move (opponent must accept), timers (implementation) + lose due to time
        24	try to find how to use HTTPS with sockets
        24	ACL (+ admins)
        25	save games in database
        26	Interface commentateur (voir plusieurs parties, et pouvoir zoomer)
        26	Mode analyse (be able to play both colors when spectate mode (interface commentateur))
(July)  27	EDIT mode
        27	create game while connected (set variant, time, THEME...)
        28	sound for chat (disabled by default), sound for moves
        28	chat : messages persistants
        29	replay games from database
        30	skin for all 2020 world championship participants, change default theme from settings page (save it into MongoDB)
(August)31	export game as PDF with SVGTOPNG, and write comments

nice to have :
- tests (Jest)
- continuous deployment to alphabal.online (merge on release if tests + lint are OK)

Checklist before deploying online next version:
- make sure socket system can automatically reconnect people to the game when they are back.
- display info when some1 leave the game or rejoin the game
- load games from past MSO (England, SK) and Musichess events
- keep the tournaments ranking page
- make sure we can still export board into PNG (and eventually create PDF of a game)
- ensure legacy routes can still be reached (/replay, /play/load/<variant>, /play/new/<coordinates>)
- ensure /replay route can still generate "static" content so robots can crawl the page content with words like "abalone" and "play"

in parallel : Python Artificial Intelligence v1 "Black Pearl"

keep an eye on these problems :
- display snackbar when game is finished instead of displaying it when user tried to interact with the interface
- replay mode : actually you can keep incrementing the move to display, meaning you can reach high value and when you click "prev" move you see no difference because you stay above the limit.
- when user changes the page, make sure he reconnects to the game (using socket) : make it like it was a disconnection (meaning you just landed on homepage with no state and you receive your current games)

later on :
  - allow correspondance play
  - allow playing "Accelium" Clash variant
  - configure copy-to-clipboard to export current board or moves played in text representation

v2:
  export game mechanisms like computeMove to an Abalone core library (instead of having these in *.utils files)