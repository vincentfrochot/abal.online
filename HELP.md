# Setup
npm install
npm run-script watch

<br />

# How to contribute

### Format project files applying the linter :
    yarn lint --fix


<br />

# Troubleshooting
### Socket messages do not reach server ?
- check nginx is running
- make sure your own certificate is trusted by your browser (e.g. load https://127.0.0.1/api/v1/user/list)

### axios http calls fail ?
- check you provided the right URL...
- check your certificate