import './app.scss';

import React, { useContext, useEffect, useState } from 'react';
import io from 'socket.io-client';
import { BrowserRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import AppRouter from './app/site/pages/AppRouter';
import Header from './app/site/components/Header/Header';
import translationsEn from './assets/translations/en.json';
import translationsDe from './assets/translations/de.json';
import translationsFr from './assets/translations/fr.json';
import translationsEs from './assets/translations/es.json';
import translationsRu from './assets/translations/ru.json';
import translationsKr from './assets/translations/kr.json';
import LanguageContext from './app/contexts/LanguageContext';
import TemplateContext from './app/contexts/TemplateContext';
import UserContext from './app/contexts/UserContext';
import { SocketContext, SOCKET_EVENTS } from './app/contexts/SocketContext';
import useSound from 'use-sound';

const translations = {
  en: translationsEn,
  de: translationsDe,
  fr: translationsFr,
  ru: translationsRu,
  es: translationsEs,
  kr: translationsKr,
};
let browserlanguage = navigator.language.split(/[-_]/)[0];

// @TODO: fix https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Missing_semicolon_before_statement for old browsers

const App = () => {
  const [playSoundMove] = useSound('static/move1.ogg');
  const [playSoundGameCreated] = useSound('static/game_start.wav');
  const [playSoundGameStarted] = useSound('static/game_start.wav');
  const [playSoundGameNewChallenger] = useSound(
    'static/game_new_challenger.wav',
    {
      sprite: {
        __default: [0, 2000],
      },
    },
  );

  const [acl, setACL] = useState([]);
  const [authenticated, setAuthenticated] = useState(false);
  const [nickname, setNickname] = useState('');
  const [language, setLanguage] = useState(
    localStorage.getItem('userlanguage')
      ? localStorage.getItem('userlanguage')
      : translations[browserlanguage] !== undefined
      ? browserlanguage
      : 'en',
  );
  const [templateGamesCol, setTemplateGamesColContext] = useState('6');
  const [hdr, setHdr] = useState(true);
  const [sounds, setSounds] = useState({
    game_created: false,
    rejoin_game: true,
    game_starts: true,
    move: false,
  });

  const templateContextValue = {
    templateGamesColContext: templateGamesCol,
    setTemplateGamesColContext: setTemplateGamesColContext,
    hdrContext: hdr,
    setHdrContext: setHdr,
    soundsContext: sounds,
    setSoundsContext: setSounds,
  };
  const languageContextValue = {
    languageContext: language,
    setLanguageContext: setLanguage,
  };
  const userContextValue = {
    userContext: {
      nickname: nickname,
      authenticated: authenticated,
      acl: acl,
    },
    setNickname,
    setAuthenticated,
    setACL,
  };
  userContextValue.userContext.isMobile =
    typeof window.orientation !== 'undefined' ||
    navigator.userAgent.indexOf('IEMobile') !== -1;

  const localSNickname = localStorage.getItem('nickname');
  if (localSNickname) {
    if (localStorage.getItem('authenticated')) {
      userContextValue.userContext.authenticated = localStorage.getItem(
        'authenticated',
      );
      if (localStorage.getItem('acl') !== null) {
        try {
          userContextValue.userContext.acl = JSON.parse(
            localStorage.getItem('acl'),
          );
        } catch (exception) {
          console.log(exception);
          localStorage.removeItem('acl');
        }
      }
    }
    userContextValue.userContext.nickname = localSNickname;
  }

  const socketContext = useContext(SocketContext);
  if (!socketContext.socket) {
    socketContext.socket = io(process.env.SOCKET_URL, {
      cookie: false,
    });

    const userData = {
      device: userContextValue.userContext.isMobile ? 'mobile' : 'laptop',
      nickname: userContextValue.userContext.nickname ?? '',
      language: language,
    };
    socketContext.socket.emit(SOCKET_EVENTS.client.user.user_log_in, userData);
  }

  useEffect(() => {
    socketContext.socket.on('pong', function (data) {
      socketContext.socket.emit('pingpong', data);
    });

    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_move_played_sound,
      () => {
        if (sounds.move) {
          playSoundMove();
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_created_sound,
      () => {
        if (sounds.game_created) {
          playSoundGameCreated();
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_new_challenger_sound,
      () => {
        if (sounds.rejoin_game) {
          playSoundGameNewChallenger();
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_started_sound,
      () => {
        if (sounds.game_starts) {
          playSoundGameStarted();
        }
      },
    );

    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_joined_you_users,
      () => {
        if (userContextValue.userContext.nickname) {
          const userData = {
            device: userContextValue.userContext.isMobile ? 'mobile' : 'laptop',
            nickname: userContextValue.userContext.nickname ?? '',
            language: language,
          };
          socketContext.socket.emit(
            SOCKET_EVENTS.client.user.user_log_in,
            userData,
          );
        }
      },
    );

    return () => {
      socketContext.socket.removeListener('pong');
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_joined_you_users,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_move_played_sound,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_created_sound,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_new_challenger_sound,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_started_sound,
      );
    };
  });

  return (
    <IntlProvider locale={language} messages={translations[language]}>
        <SocketContext.Provider value={socketContext}>
          <UserContext.Provider value={userContextValue}>
            <BrowserRouter>
              <LanguageContext.Provider value={languageContextValue}>
                <div id="appContainer">
                  <Header />
                  <TemplateContext.Provider value={templateContextValue}>
                    <AppRouter />
                  </TemplateContext.Provider>
                </div>
              </LanguageContext.Provider>
            </BrowserRouter>
          </UserContext.Provider>
        </SocketContext.Provider>
    </IntlProvider>
  );
};

export default App;
