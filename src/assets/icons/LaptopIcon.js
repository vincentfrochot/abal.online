import React from "react";

const LaptopIcon = ({
    style = {},
    fill = "#000",
    className = "",
    viewBox = "0 0 40 40",
    size = "30",
    x = "0",
    y = "0",
    clickHandler = () => {},
    overHandler = () => {},
    outHandler = () => {}
}) => (
    <svg
        width={size}
        height={size}
        style={style}
        viewBox={viewBox}
        x={x}
        y={y}
        xmlns="http://www.w3.org/2000/svg"
        className={"svg-icon " + (className || "")}
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <a
            cursor="pointer"
            pointerEvents="all"
            onClick={clickHandler}
            onMouseOver={overHandler}
            onMouseOut={outHandler}
        >
            <rect x="0" y="0" width="100%" height="100%" fill="none" />
            <path
            fill={fill}
            d="M37.1,2H2.9C1.3,2,0,3.3,0,5v24.6c0,1.6,1.3,2.9,2.9,2.9h11.9L14.1,35c-0.3,1-1.4,1.8-2.4,1.8H7.9c-0.3,0-0.6,0.3-0.6,0.6
		c0,0.3,0.3,0.6,0.6,0.6h24.2c0.3,0,0.6-0.3,0.6-0.6c0-0.3-0.3-0.6-0.6-0.6h-3.8c-1,0-2.1-0.8-2.4-1.8l-0.7-2.5h11.9
		c1.6,0,2.9-1.3,2.9-2.9V5C40,3.3,38.7,2,37.1,2z M2.9,27.8c-0.3,0-0.6-0.3-0.6-0.6v-8.6v-1.4V5c0-0.3,0.3-0.6,0.6-0.6h34.1
		c0.3,0,0.6,0.3,0.6,0.6v22.3c0,0.3-0.3,0.6-0.6,0.6H2.9z M34.8,30.2c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6
		c0.3,0,0.6,0.3,0.6,0.6C35.4,29.9,35.1,30.2,34.8,30.2z M37.1,30.2c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6
		c0.3,0,0.6,0.3,0.6,0.6C37.7,29.9,37.4,30.2,37.1,30.2z"/>
        </a>
    </svg>
);

export default LaptopIcon;
