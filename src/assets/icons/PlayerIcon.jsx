import React from 'react';

const PlayerIcon = ({
  style = {},
  fill = '#6299ff',
  className = '',
  viewBox = '0 0 512 384',
  size = '30',
  x = '0',
  y = '0',
  clickHandler = () => {},
}) => (
  <svg
    width={size}
    height={size}
    style={style}
    viewBox={viewBox}
    x={x}
    y={y}
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${className || ''}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path fill={fill} xmlns="http://www.w3.org/2000/svg" d="M120 72c0-39.765 32.235-72 72-72s72 32.235 72 72c0 39.764-32.235 72-72 72s-72-32.236-72-72zm254.627 1.373c-12.496-12.497-32.758-12.497-45.254 0L242.745 160H141.254L54.627 73.373c-12.496-12.497-32.758-12.497-45.254 0-12.497 12.497-12.497 32.758 0 45.255L104 213.254V480c0 17.673 14.327 32 32 32h16c17.673 0 32-14.327 32-32V368h16v112c0 17.673 14.327 32 32 32h16c17.673 0 32-14.327 32-32V213.254l94.627-94.627c12.497-12.497 12.497-32.757 0-45.254z" />
    <rect x="0" y="0" width="100%" height="100%" fill="none" />
  </svg>
);

export default PlayerIcon;
