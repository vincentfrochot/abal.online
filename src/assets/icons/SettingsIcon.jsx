import React from 'react';

const SettingsIcon = ({ parent }) => (
  <svg
    width={30}
    height={30}
    style={{}}
    viewBox="0 0 40 40"
    x={0}
    y={0}
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${parent || ''}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <rect x="0" y="0" width="100%" height="100%" fill="none" />
    <path
      className={`${parent}settings-icon`}
      d="M36.1,24.9l3.5-0.9c0.3-1.3,0.4-2.7,0.4-4.1c0-1.4-0.1-2.8-0.4-4.1l-3.5-0.9c-1.3-0.3-1.9-1.8-1.2-3l1.8-3
		c-1.5-2.3-3.5-4.3-5.8-5.8l-3,1.8c-1.2,0.7-2.7,0.1-3-1.2l-0.9-3.5C22.8,0.1,21.4,0,20,0s-2.8,0.1-4.1,0.4l-0.9,3.5
		c-0.3,1.3-1.8,1.9-3,1.2l-3-1.8C6.8,4.8,4.8,6.8,3.3,9.1l1.8,3c0.7,1.2,0.1,2.7-1.2,3l-3.5,0.9C0.1,17.2,0,18.6,0,20
		c0,1.4,0.1,2.8,0.4,4.1l3.5,0.9c1.3,0.3,1.9,1.8,1.2,3l-1.8,3c1.5,2.3,3.5,4.3,5.8,5.8l3-1.8c1.2-0.7,2.7-0.1,3,1.2l0.9,3.5
		c1.3,0.3,2.7,0.4,4.1,0.4s2.8-0.1,4.1-0.4l0.9-3.5c0.3-1.3,1.8-1.9,3-1.2l3,1.8c2.3-1.5,4.3-3.5,5.8-5.8l-1.8-3
		C34.2,26.8,34.8,25.3,36.1,24.9z M20,30.6c-5.9,0-10.6-4.8-10.6-10.6c0-5.9,4.8-10.6,10.6-10.6c5.9,0,10.6,4.8,10.6,10.6
		S25.9,30.6,20,30.6z"
    />
  </svg>
);

export default SettingsIcon;
