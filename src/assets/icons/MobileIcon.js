import React from "react";

const MobileIcon = ({
    style = {},
    fill = "#000",
    className = "",
    viewBox = "0 0 40 40",
    size = "30",
    x = "0",
    y = "0",
    clickHandler = () => {},
    overHandler = () => {},
    outHandler = () => {}
}) => (
    <svg
        width={size}
        height={size}
        style={style}
        viewBox={viewBox}
        x={x}
        y={y}
        xmlns="http://www.w3.org/2000/svg"
        className={"svg-icon " + (className || "")}
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <a
            cursor="pointer"
            pointerEvents="all"
            onClick={clickHandler}
            onMouseOver={overHandler}
            onMouseOut={outHandler}
        >
            <rect x="0" y="0" width="100%" height="100%" fill="none" />
            <path
                fill={fill}
                d="M24.7,6.3h-9.3c-1.6,0-2.9,1.3-2.9,2.9v21.7c0,1.6,1.3,2.9,2.9,2.9l9.3,0c1.6,0,2.9-1.3,2.9-2.9l0-21.7
		C27.5,7.6,26.2,6.3,24.7,6.3z M18.7,8h2.7c0.2,0,0.4,0.2,0.4,0.4c0,0.2-0.2,0.4-0.4,0.4h-2.7c-0.2,0-0.4-0.2-0.4-0.4
		C18.3,8.2,18.4,8,18.7,8z M17.1,8.1c0.2-0.2,0.4-0.2,0.6,0c0.1,0.1,0.1,0.2,0.1,0.3c0,0.1,0,0.2-0.1,0.3c-0.1,0.1-0.2,0.1-0.3,0.1
		c-0.1,0-0.2,0-0.3-0.1c-0.1-0.1-0.1-0.2-0.1-0.3C16.9,8.3,17,8.2,17.1,8.1z M19.9,32.9c-0.7,0-1.3-0.6-1.3-1.3
		c0-0.7,0.6-1.3,1.3-1.3s1.3,0.6,1.3,1.3C21.2,32.3,20.7,32.9,19.9,32.9z M26.2,29.4H14V10.4h12.2V29.4L26.2,29.4z"
            />
        </a>
    </svg>
);

export default MobileIcon;
