import React from 'react';

const ReplayIcon = ({
  parent = '',
  style = {},
  fill = '#fff',
  className = '',
  viewBox = '0 0 40 40',
  size = '30',
  x = '0',
  y = '0',
  clickHandler = () => {},
  overHandler = () => {},
  outHandler = () => {},
}) => (
  <svg
    width={size}
    height={size}
    style={style}
    viewBox={viewBox}
    x={x}
    y={y}
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${className || ''}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      className={`${parent}replay-icon`}
      d="M21.4,24.7l-5.5,3.2c-1.2,0.7-2.7-0.2-2.7-1.5V20v-6.4c0-1.4,1.5-2.2,2.7-1.5l5.5,3.2l5.5,3.2c1.2,0.7,1.2,2.4,0,3.1
      L21.4,24.7z M39.7,18.8L30.9,3.6c-0.4-0.8-1.3-1.2-2.1-1.2H11.2c-0.9,0-1.7,0.5-2.1,1.2L0.3,18.8c-0.4,0.8-0.4,1.7,0,2.5l3.4,5.9
      l-2.6,1.5l8.3,4.8l0-9.6l-2.6,1.5L3.7,20l8.1-14.1h16.3L36.3,20l-8.1,14.1H20c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h8.8
      c0.9,0,1.7-0.5,2.1-1.2l8.8-15.2C40.1,20.5,40.1,19.5,39.7,18.8z"
    />
    <rect x="0" y="0" width="100%" height="100%" fill="none" />
  </svg>
);

export default ReplayIcon;
