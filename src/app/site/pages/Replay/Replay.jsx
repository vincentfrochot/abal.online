import './replay.scss';

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  Button,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import {
  viewBoxPredator,
  viewBoxGuardian,
} from '../../../games/abalone/components/Hexagon/hexagon.utils';
import { ReplayGame } from '../../../games/abalone/components/ReplayGame/ReplayGame';
import { BoardDefinition } from '../../../games/abalone/components/Board/BoardDefinition';
import { FormattedMessage } from 'react-intl';

const Replay = () => {
  const [games, setGames] = useState([]);
  const [game, setGame] = useState(null);
  const [moveIndex, setMoveIndex] = useState(0);
  const [playerNickname, setPlayerNickname] = useState('');
  const [histoPage, setHistoPage] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios(
        // 'https://127.0.0.1/api/v1/game/abalone/list',
        // 'https://www.abalone.games/api/v1/game/abalone/list',
        `${process.env.API_URL}/api/v1/game/abalone/list`,
      );

      setGames([...response.data.content]);
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      if (playerNickname.length >= 3) {
        const response = await axios(
          // 'https://127.0.0.1/api/v1/game/abalone/list',
          // 'https://www.abalone.games/api/v1/game/abalone/list',
          `${process.env.API_URL}/api/v1/game/abalone/list`,
          {
            params: {
              nickname: playerNickname,
              page: histoPage,
            },
          },
        );

        setGames([...response.data.content]);
      } else {
        const response = await axios(
          // 'https://127.0.0.1/api/v1/game/abalone/list',
          // 'https://www.abalone.games/api/v1/game/abalone/list',
          `${process.env.API_URL}/api/v1/game/abalone/list`,
          {
            params: {
              page: histoPage,
            },
          },
        );

        setGames([...response.data.content]);
      }
    };

    fetchData();
  }, [playerNickname, histoPage]);

  const handleGameClick = (gameId) => {
    const fetchData = async () => {
      const response = await axios(
        // 'https://127.0.0.1/api/v1/game/abalone/match',
        // 'https://www.abalone.games/api/v1/game/abalone/match',
        `${process.env.API_URL}/api/v1/game/abalone/match`,
        { params: { gameId: gameId } },
      );
      setMoveIndex(0);
      setGame(response.data.content);
    };

    fetchData();
  };

  const moveBackward = () => {
    setMoveIndex((prev) => {
      if (!prev) {
        return 0;
      }
      return prev - 1;
    });
  };

  const moveForward = () => {
    if (game) {
      setMoveIndex((prev) => {
        if (prev >= game.histoScores.length - 1) {
          return prev;
        }
        return prev + 1;
      });
    }
  };

  const moveToFirst = () => {
    setMoveIndex(0);
  };

  const moveToLast = () => {
    if (game) {
      setMoveIndex(game.histoScores.length - 1);
    }
  };

  const handlePlayerNicknameChange = (e) => {
    setPlayerNickname(e.target.value);
  };

  const handleHistoPageChange = (e) => {
    setHistoPage(e.target.value);
  };

  let moves = '';
  if (game) {
    moves = moveIndex + '/' + (game.histoScores.length - 1);
  }

  return (
    <div id="divContainer">
      <Grid container id="gridContainer">
        <BoardDefinition />
        <FormattedMessage id="app.page.replay--introduction-text" />
        <Grid item xs={12} style={{ margin: '0px', padding: '5px' }}>
          <Paper
            className="gamecard-paper"
            style={{
              margin: '0px',
              padding: '0px',
              backgroundColor: '#ddd',
              border: '#f00',
            }}
          >
            <svg viewBox={'-100 0 700 320'} preserveAspectRatio="xMinYMin meet">
              <ReplayGame
                key={game?._id || 'pending'}
                displayedMoveIndex={moveIndex}
                game={game}
              />
            </svg>
            {moves}
            <Button onClick={moveToFirst}>
              <FormattedMessage id="app.page.home.game--first-button" />
            </Button>
            <Button onClick={moveBackward}>
              <FormattedMessage id="app.page.home.game--prev-button" />
            </Button>
            <Button onClick={moveForward}>
              <FormattedMessage id="app.page.home.game--next-button" />
            </Button>
            <Button onClick={moveToLast}>
              <FormattedMessage id="app.page.home.game--last-button" />
            </Button>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="standard-basic"
            label="filter by player"
            value={playerNickname}
            onChange={handlePlayerNicknameChange}
          />
          <FormControl style={{ margin: 4, minWidth: 120 }}>
            <InputLabel htmlFor="page-number">page</InputLabel>
            <Select
              native
              value={histoPage}
              onChange={handleHistoPageChange}
              inputProps={{
                id: 'page-number',
              }}
            >
              <option value={1}>1</option>
              <option value={2}>2</option>
              <option value={3}>3</option>
              <option value={4}>4</option>
              <option value={5}>5</option>
              <option value={6}>6</option>
              <option value={7}>7</option>
              <option value={8}>8</option>
              <option value={9}>9</option>
              <option value={10}>10</option>
            </Select>
          </FormControl>
        </Grid>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <FormattedMessage id="app.page.replay--date" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="app.page.replay--black" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="app.page.replay--white" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="app.page.replay--score" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="app.page.replay--number-of-moves" />
              </TableCell>
              <TableCell>
                <FormattedMessage id="app.page.replay--variant" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {games.map((g) => (
              <TableRow
                key={g.id}
                onClick={(e) => handleGameClick(g.id)}
                className={game && g.id === game._id ? 'selected-game' : ''}
              >
                <TableCell>{g.date}</TableCell>
                <TableCell>
                  {g.playersNicknames[0] === g.winner ? (
                    <b style={{ color: 'green' }}>{g.winner}</b>
                  ) : (
                    g.playersNicknames[0]
                  )}
                </TableCell>
                <TableCell>
                  {g.playersNicknames[1] === g.winner ? (
                    <b style={{ color: 'green' }}>{g.winner}</b>
                  ) : (
                    g.playersNicknames[1]
                  )}
                </TableCell>
                <TableCell>
                  {g.score[0]} - {g.score[1]}
                </TableCell>
                <TableCell>{g.gameLength}</TableCell>
                <TableCell>{g.variantName}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
    </div>
  );
};
export default Replay;
