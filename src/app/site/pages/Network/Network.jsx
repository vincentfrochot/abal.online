import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import { Card, Paper, Grid } from '@material-ui/core';
import { SocketContext } from '../../../contexts/SocketContext';

const Network = () => {
  const socketContext = useContext(SocketContext);

  const handleChat = () => {
    socketContext.socket.emit("reset_chat");
  }

  return (
    <Grid>
      <Paper xs={12}>
        <FormattedMessage id="app.page.network--application" />
        <br />
        <a
          href="https://games-up.fr/index.php/2015/10/24/abalone-application/"
          target="_blank"
        >
          <img src="./static/abalone_application.jpg" />
        </a>
      </Paper>
      <br />
      <Paper xs={12}>
        <FormattedMessage id="app.page.network--blog" />
        <br />
        <a href="https://abaloneonline.wordpress.com/" target="_blank">
          <img src="./static/onlineabalone_wordpress.jpg" />
        </a>
      </Paper>
      <br />
      <Paper xs={12}>
        <FormattedMessage id="app.page.network--mindsportsolympiad" />
        <br />
        <a href="https://msoworld.com" target="_blank">
          <img src="./static/mindsportsolympiad.jpeg" />
        </a>
      </Paper>
      <button onClick={handleChat}></button>
    </Grid>
  );
};

export default Network;
