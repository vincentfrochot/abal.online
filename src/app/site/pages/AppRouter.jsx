import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Preferences from './Preferences/Preferences';
import Home from './Home/Home';
import Replay from './Replay/Replay';
import Network from './Network/Network';

const AppRouter = () => (
  <Switch>
    <Route path="/preferences">
      <Preferences />
    </Route>
    <Route path="/replay">
      <Replay />
    </Route>
    <Route path="/network">
      <Network />
    </Route>
    <Route path="/">
      <Home />
    </Route>
  </Switch>
);

export default AppRouter;
