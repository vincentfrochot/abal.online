import React from 'react';
import { FormattedMessage } from 'react-intl';
import LanguageSelector from '../../components/Preferences/LanguageSelector/LanguageSelector';
import TemplateSelector from '../../components/Preferences/TemplateSettings/TemplateSelector';
import HdrToggler from '../../components/Preferences/HdrToggler/HdrToggler';
import SoundsSettings from '../../components/Preferences/SoundsSettings/SoundsSettings';

const Preferences = () => (
  <form>
    <LanguageSelector />
    <br />
    <br />
    <TemplateSelector />
    <br />
    <br />
    <HdrToggler />
    <br />
    <br />
    <SoundsSettings />
    {/* <SoundsSelector />
    <BoardThemeSelector /> */}
  </form>
);

export default Preferences;
