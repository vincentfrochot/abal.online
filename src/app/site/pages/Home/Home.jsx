import './home.scss';

import React, { useState, useContext, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { Grid, Paper, Button, Card } from '@material-ui/core';
import { GameCard } from '../../../games/abalone/components/GameCard/GameCard';
import { BoardDefinition } from '../../../games/abalone/components/Board/BoardDefinition';
import { OnlinePlayerList } from '../../../games/core/components/OnlinePlayerList/OnlinePlayerList';
import { OnlineMatchList } from '../../../games/core/components/OnlineMatchList/OnlineMatchList';
import { CreateGameDialog } from '../../../games/core/components/CreateGameDialog/CreateGameDialog';
import UserContext from '../../../contexts/UserContext';
import { Chat } from '../../components/Chat/Chat';
import GamesContext from '../../../contexts/GamesContext';
import { SOCKET_EVENTS, SocketContext } from '../../../contexts/SocketContext';
import { ConnectionDialog } from '../../components/ConnectionDialog/ConnectionDialog';
import axios from 'axios';
import TemplateContext from '../../../contexts/TemplateContext';

const Home = () => {
  const { userContext, setAuthenticated, setNickname, setACL } = useContext(
    UserContext,
  );
  const socketContext = useContext(SocketContext);

  const [games, setGames] = useState([]); // games to display (current user is inside these games)

  const initialDisplayChat = true; // templateContext.displayChat;
  const initialDisplayUsers = true; // templateContext.displayChat;
  const [displayChat, setDisplayChat] = useState(initialDisplayChat);
  const [displayUsers, setDisplayUsers] = useState(initialDisplayUsers);
  const [showBanner, setShowBanner] = useState(true);

  const [displayNewGameModal, setDisplayNewGameModal] = useState(false);

  const [displayLogin, setDisplayLogIn] = useState(false);
  const [feedbackLogin, setFeedbackLogin] = useState(null);

  useEffect(() => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.games_init);

    socketContext.socket.on(SOCKET_EVENTS.server.game.games_init, (data) => {
      if (data.content.games) {
        setGames(data.content.games);
      }
    });

    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_logged_in_games,
      (data) => {
        if (data.content.games) {
          setGames(data.content.games);
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_logged_out_you,
      () => {
        setGames([]);
      },
    );
    socketContext.socket.on(SOCKET_EVENTS.server.game.game_created, (data) => {
      setGames((games) => [...games, data.content.game]);
    });

    socketContext.socket.on(SOCKET_EVENTS.server.game.game_watched, (data) => {
      const game = data.content.game;
      setGames((previous) => {
        const existingGame = previous.find((e) => game.id === e.id);
        if (existingGame !== undefined) {
          return previous.map((element) => {
            return element.id === game.id ? game : element;
          });
        } else {
          return [...previous, game];
        }
      });
    });
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_challenged,
      (data) => {
        const game = data.content.game;

        // setGames(data.content); // @TODO: déterminer COMMENT gérer un REMPLACEMENT d'élément dans le setState ???
        // see https://stackoverflow.com/questions/29537299/react-how-to-update-state-item1-in-state-using-setstate
        // https://stackoverflow.com/questions/28121272/whats-the-best-way-to-update-an-object-in-an-array-in-reactjs
        // setGames({
        //   data: data.map(el => (el.id === id ? {...el, text} : el))
        // });
        // @TODO: mapping !!!

        // if (templateContext.soundsContext.rejoin_game) {
        //   playSoundNewChallenger('badum');
        // }
        setGames((previous) => {
          return previous.map((element) => {
            return element.id === game.id ? game : element;
          });
        });
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_canceled_challenge,
      (data) => {
        const game = data.content.game;
        if (game) {
          setGames((previous) => {
            const existingGame = previous.find((e) => game.id === e.id);
            if (existingGame !== undefined) {
              return [
                ...previous.filter((e) => e.id !== existingGame.id),
                game,
              ];
            }
          });
        }
      },
    );
    socketContext.socket.on(SOCKET_EVENTS.server.game.game_started, (data) => {
      const game = data.content.game;
      setGames((previous) => {
        const existingGame = previous.find((e) => game.id === e.id);
        if (existingGame !== undefined) {
          // if (templateContext.soundsContext.game_starts) {
          //   playSoundGameStarted();
          // }
          return previous.map((element) => {
            return element.id === game.id ? game : element;
          });
        } else {
          return [...previous, game]; // @TODO: check
        }
      });
    });
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_move_played,
      (data) => {
        const game = data.content.game;
        setGames((previous) => {
          const existingGame = previous.find((e) => game.id === e.id);
          if (existingGame !== undefined) {
            // if (templateContext.soundsContext.move) {
            //   playSoundMove();
            // }
            return previous.map((element) => {
              return element.id === game.id ? game : element;
            });
          } else {
            return [...previous, game]; // @TODO: check
          }
        });
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_move_undone,
      (data) => {
        const game = data.content.game;
        setGames((previous) => {
          const existingGame = previous.find((e) => game.id === e.id);
          if (existingGame !== undefined) {
            return previous.map((element) => {
              return element.id === game.id ? game : element;
            });
          } else {
            return [...previous, game];
          }
        });
      },
    );
    socketContext.socket.on(SOCKET_EVENTS.server.game.game_quited, (data) => {
      setGames((games) => games.filter((e) => e.id !== data.content.id));
    });
    socketContext.socket.on(SOCKET_EVENTS.server.game.game_won, (data) => {
      const game = data.content.game;
      setGames((previous) => {
        const existingGame = previous.find((e) => game.id === e.id);
        if (existingGame !== undefined) {
          return previous.map((element) => {
            return element.id === game.id ? game : element;
          });
        } else {
          return [...previous, game];
        }
      });
    });
    socketContext.socket.on(SOCKET_EVENTS.server.game.games_to_end, (data) => {
      const games = data.content.games;
      setGames((previous) => {
        return previous.map((element) => {
          const gameToEnd = games.find((game) => game.id === element.id);
          if (gameToEnd !== undefined) {
            return gameToEnd;
          }
          return element;
        });
      });
    });
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.game_doesnt_exist,
      (data) => {
        setGames((games) =>
          games.filter((game) => game.id !== data.content.gameId),
        );
      },
    );

    return () => {
      socketContext.socket.removeListener(SOCKET_EVENTS.server.game.games_init);
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_logged_in_games,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_logged_out_you,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_created,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_watched,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_challenged,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_canceled_challenge,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_started,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_move_played,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_move_undone,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_quited,
      );
      socketContext.socket.removeListener(SOCKET_EVENTS.server.game.game_won);
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.games_to_end,
      );
    };
  }, []);

  const gamesContextValue = {
    gamesContext: {
      games: games,
    },
    setGames,
  };

  const quitGame = (identifier) => {
    setGames((previous) => {
      const newGames = [...previous].filter((item) => item.id !== identifier);
      return newGames;
    });
    socketContext.socket.emit('game_quit', { gameId: identifier });
  };

  const toggleChatHandler = () => {
    setDisplayChat((previous) => !previous);
  };

  const toggleUsersHandler = () => {
    setDisplayUsers((previous) => !previous);
  };

  const openNewGameModal = () => {
    if (userContext.authenticated) {
      setDisplayNewGameModal(true);
    } else {
      setDisplayLogIn(true);
    }
  };

  const closeLogin = () => {
    setDisplayLogIn(false);
  };

  const hideNewGameModal = () => {
    setDisplayNewGameModal(false);
  };

  const submitNewGameModal = (newGameParams) => {
    socketContext.socket.emit('game_create', newGameParams);
  };

  const submitLogin = (data) => {
    if (data.email && data.nickname && data.password) {
      postData(data);
    }
  };

  const postData = async (data) => {
    await axios
      .post(process.env.API_URL + '/api/v1/user/login', data)
      .then((response) => {
        const nickname = response.data.content.nickname;
        const acl = response.data.content.acl;
        if (nickname) {
          setAuthenticated(true);
          setFeedbackLogin('');
          setNickname(nickname);
          if (acl) {
            localStorage.setItem('acl', JSON.stringify(acl));
            setACL(JSON.stringify(acl));
          }
          localStorage.setItem('authenticated', true);
          localStorage.setItem('nickname', nickname);
          socketContext.socket.emit('user_log_in', {
            // device:, TODO VFR: check if we need to send device and language from here also
            nickname,
            // language:,
          });
          closeLogin();
        } else {
          console.log('no nickname in response...');
        }
      })
      .catch(function (error) {
        if (error.response) {
          if (error.response.data) {
            setFeedbackLogin(error.response.data.error);
          }
        } else {
          setFeedbackLogin('');
        }
      });
  };

  const chatCols = displayChat ? 2 : games.length === 0 ? 2 : 0;
  const listsCols = displayUsers
    ? games.length === 0 && !showBanner
      ? 10
      : 3
    : 0;
  const gamesCols = 12 - chatCols - listsCols;

  return (
    <div id="divContainer">
      <Grid container id="gridContainer">
        <BoardDefinition />
        <Grid item xs={2} className="subheader">
          <Button onClick={toggleChatHandler}>
            <FormattedMessage id="app.page.home.players--show-chat--button" />
          </Button>
        </Grid>

        <Grid item xs={7} className="subheader">
          <Button
            className="button-play"
            onClick={openNewGameModal}
            size="small"
          >
            <FormattedMessage id="app.page.home.players--create-new-game-button" />
          </Button>
        </Grid>

        <Grid item xs={3} className="subheader">
          <Button onClick={toggleUsersHandler}>
            <FormattedMessage id="app.page.home.players--show-users-matches--button" />
          </Button>
        </Grid>

        {games.length === 0 && (
          <Grid id="chatContainer" item xs={chatCols} className="chatContainer">
            {displayChat ? <Chat></Chat> : <></>}
          </Grid>
        )}

        {games.length > 0 && displayChat && (
          <Grid id="chatContainer" item xs={chatCols} className="chatContainer">
            <Chat></Chat>
          </Grid>
        )}

        {games.length === 0 && showBanner && (
          <Grid
            id="chatContainer"
            item
            xs={7}
            style={{
              paddingLeft: '20px',
              paddingRight: '20px',
              paddingTop: '20px',
            }}
            className="chatContainer"
          >
            <a
              href="https://msoworld.com/product/abalone-world-championship/"
              target="_blank"
            >
              <img src="./static/header_mso.gif" style={{ maxWidth: '100%' }} />
            </a>
          </Grid>
        )}

        <GamesContext.Provider value={gamesContextValue}>
          {games.length > 0 && gamesCols > 0 && (
            <Grid
              container
              direction="row"
              item
              xs={gamesCols}
              className="gameContainer"
            >
              {games.map((element, index) => (
                <GameCard
                  key={`gamecard-${element.id}-${element.lastMoveIndex}`}
                  quitGameHandler={quitGame}
                  multipleGames={gamesCols === 12}
                  sidentifier={element.id}
                />
              ))}
            </Grid>
          )}

          {displayUsers && (
            <Grid item xs={listsCols}>
              <OnlinePlayerList />
              <OnlineMatchList />
            </Grid>
          )}
        </GamesContext.Provider>

        <ConnectionDialog
          open={displayLogin}
          submitFunction={submitLogin}
          closeFunction={closeLogin}
          feedbackLogin={feedbackLogin}
          setFeedbackLogin={setFeedbackLogin}
        />

        <CreateGameDialog
          open={displayNewGameModal}
          closeFunction={hideNewGameModal}
          submitFunction={submitNewGameModal}
          aria-labelledby="form-dialog-title"
        ></CreateGameDialog>
      </Grid>
    </div>
  );
};
export default Home;
