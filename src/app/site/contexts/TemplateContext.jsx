import React from 'react';

const TemplateContext = React.createContext({
  displayChat: true,
  displayUsers: true,
});

export default TemplateContext;
