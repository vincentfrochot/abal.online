import './header.scss';

import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import LogoIconBeta from '../../../../assets/icons/LogoIconBeta';
import SettingsIcon from '../../../../assets/icons/SettingsIcon';
import ReplayIcon from '../../../../assets/icons/ReplayIcon';
import { Login } from '../Login/Login';
import { FormattedMessage } from 'react-intl';
import NetworkIcon from '../../../../assets/icons/NetworkIcon';
import { SocketContext, SOCKET_EVENTS } from '../../../contexts/SocketContext';

const Header = () => {
  const [usersCardinal, setUsersCardinal] = useState(0);
  
  const socketContext = useContext(SocketContext);

  // number_of_users
  useEffect(() => {
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.number_of_users,
      handleInit,
    );

    return () => {
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.number_of_users,
        handleInit,
      );
    };
  }, []);

  const handleInit = (data) => {
    const cardinal = data.content.cardinal;
    if (cardinal) {
      setUsersCardinal(cardinal);
    }
  };

  return (
    <div style={{ display: 'flex' }}>
      <AppBar position="static" id="header" className="header">
        <Toolbar id="headerToolbar">
          <div id="logoContainer">
            <Link to="/">
              <LogoIconBeta parent="header__" />
            </Link>
          </div>
          <Link to="/replay">
            <ReplayIcon parent="header__" />
          </Link>
          <Link to="/network">
            <NetworkIcon parent="header__" />
          </Link>
          <Link to="/preferences">
            <SettingsIcon parent="header__" />
          </Link>
          <div className="header__number-of-users-text">
            <Typography>
              {usersCardinal}{' '}
              <FormattedMessage id="app.header--online-users" />
            </Typography>
          </div>
          <Login />
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
