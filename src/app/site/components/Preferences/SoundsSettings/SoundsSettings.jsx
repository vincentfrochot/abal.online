import React, { useContext, useEffect, useState, useRef } from 'react';
import TemplateContext from '../../../../contexts/TemplateContext';
import { FormattedMessage } from 'react-intl';
import { FormGroup, FormControlLabel, Switch } from '@material-ui/core';

const SoundsSettings = () => {
  const firstRender = useRef(true); // allow using useEffect for modifications only (check "current" property of firstRender in the UseEffect)
  const { soundsContext, setSoundsContext } = useContext(TemplateContext);
  const [sounds, setSounds] = useState(soundsContext);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      setSoundsContext(sounds);
    }
  }, [sounds]);

  const handleChange = (soundToChange) => {
    if (soundToChange === 'move') {     
      setSounds((previous) => {
        return {
          game_created: previous.game_created,
          rejoin_game: previous.rejoin_game,
          game_starts: previous.game_starts,
          move: !previous.move,
        };
      });
    } else if (soundToChange === 'game_created') {
      setSounds((previous) => {
        return {
          game_created: !previous.game_created,
          rejoin_game: previous.rejoin_game,
          game_starts: previous.game_starts,
          move: previous.move,
        };
      });

    } else if (soundToChange === 'rejoin_game') {
      setSounds((previous) => {
        return {
          game_created: previous.game_created,
          rejoin_game: !previous.rejoin_game,
          game_starts: previous.game_starts,
          move: previous.move,
        };
      });
    } else if (soundToChange === 'game_starts') {
      setSounds((previous) => {
        return {
          game_created: previous.game_created,
          rejoin_game: previous.rejoin_game,
          game_starts: !previous.game_starts,
          move: previous.move,
        };
      });
    }
  };

  return (
    <>
      <FormGroup >
        Sounds: <br />
        <FormControlLabel
          control={
            <Switch
              checked={sounds.game_created}
              onChange={(e) => handleChange('game_created', e)}
              value={sounds.game_created}
              color="primary"
            />
          }
          label={
            "game creation : " + (sounds.game_created ? 'ON' : 'OFF')
          }
        />
        <br />
        <FormControlLabel
          control={
            <Switch
              checked={sounds.rejoin_game}
              onChange={(e) => handleChange('rejoin_game', e)}
              value={sounds.rejoin_game}
              color="primary"
            />
          }
          label={
            "rejoin game: " + (sounds.rejoin_game ? 'ON' : 'OFF')
          }
        />
        <br />
        <FormControlLabel
          control={
            <Switch
              checked={sounds.game_starts}
              onChange={(e) => handleChange('game_starts', e)}
              value={sounds.game_starts}
              color="primary"
            />
          }
          label={
            "game starts: " + (sounds.game_starts ? 'ON' : 'OFF')
          }
        />
        <br />
        <FormControlLabel
          control={
            <Switch
              checked={sounds.move}
              onChange={(e) => handleChange('move', e)}
              value={sounds.move}
              color="primary"
            />
          }
          label={
            'moves: ' + (sounds.move ? 'ON' : 'OFF')
          }
        />
 
      </FormGroup>
    </>
  );
};

export default SoundsSettings;
