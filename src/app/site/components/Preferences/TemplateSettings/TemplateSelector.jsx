import React, {
  useContext, useEffect, useState, useRef,
} from 'react';
import TemplateContext from '../../../../contexts/TemplateContext';
import { FormattedMessage } from 'react-intl';

const TemplateSelector = () => {
  const firstRender = useRef(true); // allow using useEffect for modifications only (check "current" property of firstRender in the UseEffect)
  const { templateGamesColContext, setTemplateGamesColContext } = useContext(TemplateContext);
  const [col, setCol] = useState(templateGamesColContext);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      setTemplateGamesColContext(col);
    }
  }, [col]);

  const changeCol = (event) => {
    setCol(event.target.value);
  };

  return (
    <>
      <select onChange={(e) => changeCol(e)} value={col}>
        <option value="6">2</option>
        <option value="4">3</option>
        <option value="3">4</option>
        <option value="2">6</option>
      </select>
      <FormattedMessage id="app.page.preferences--games-per-line" />
    </>
  );
};

export default TemplateSelector;
