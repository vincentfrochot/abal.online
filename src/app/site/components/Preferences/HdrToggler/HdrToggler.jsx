import React, {
  useContext, useEffect, useState, useRef,
} from 'react';
import TemplateContext from '../../../../contexts/TemplateContext';
import { FormattedMessage } from 'react-intl';
import { FormGroup, FormControlLabel, Switch } from '@material-ui/core';

const HdrToggler = () => {
  const firstRender = useRef(true); // allow using useEffect for modifications only (check "current" property of firstRender in the UseEffect)
  const { hdrContext, setHdrContext } = useContext(TemplateContext);
  const [hdr, setHdr] = useState(hdrContext);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      setHdrContext(hdr);
    }
  }, [hdr]);

  const handleChange = () => {
    setHdr((previous) => !previous);
  };

  return (
    <>
      <FormGroup row>
      <FormControlLabel
        control={<Switch checked={hdrContext} onChange={handleChange} name="hdrSwitch" />}
        label="HDR"
      />
      </FormGroup>
    </>
  );
};

export default HdrToggler;
