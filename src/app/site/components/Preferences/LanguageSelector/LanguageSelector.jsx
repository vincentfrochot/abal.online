import React, {
  useContext, useEffect, useState, useRef,
} from 'react';
import LanguageContext from '../../../../contexts/LanguageContext';

const LanguageSelector = () => {
  const firstRender = useRef(true); // allow using useEffect for modifications only (check "current" property of firstRender in the UseEffect)
  const { languageContext, setLanguageContext } = useContext(LanguageContext);
  const [language, setLanguage] = useState(languageContext);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      localStorage.setItem('userlanguage', language);
      setLanguageContext(language);
    }
  }, [language]);

  const changeLanguage = (event) => {
    setLanguage(event.target.value);
  };

  return (
    <>
      <select onChange={(e) => changeLanguage(e)} value={language}>
        <option value="en">English</option>
        <option value="fr">Français</option>
        <option value="de">Deutsch</option>
        <option value="ru">Pусский</option>
        <option value="es">Español</option>
        <option value="kr">한국어</option>
      </select>
      {' '}
      Language missing ? Help us translate (https://gitlab.com/vincentfrochot/abal.online)
    </>
  );
};

export default LanguageSelector;
