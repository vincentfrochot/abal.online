import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  FormControl,
  IconButton,
  InputAdornment,
  Input,
  InputLabel,
} from '@material-ui/core';
import { useState } from 'react';
import VisibilityIcon from '../../../../assets/icons/VisibilityIcon';
import VisibilityOffIcon from '../../../../assets/icons/VisibilityOffIcon';
import { FormattedMessage } from 'react-intl';

export const ConnectionDialog = ({
  open,
  submitFunction,
  closeFunction,
  feedbackLogin,
  setFeedbackLogin,
}) => {
  const [email, setEmail] = useState('');
  const [nickname, setNickname] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const handleChange = (fieldName, event) => {
    if (fieldName === 'email') {
      setEmail(event.target.value);
    }

    if (fieldName === 'nickname') {
      setNickname(event.target.value);
    }

    if (fieldName === 'password') {
      setPassword(event.target.value);
    }
  };

  const close = () => {
    setFeedbackLogin('');
    closeFunction();
  };

  const submit = () => {
    submitFunction(toJSON());
  };

  const toJSON = () => {
    return {
      email,
      nickname,
      password,
    };
  };

  const changeShowPassword = (value) => {
    setShowPassword((previous) => !previous);
  };

  return (
    <Dialog open={open} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">
        <FormattedMessage id="app.login.sign-in" /> /{' '}
        <FormattedMessage id="app.login.sign-up" />
      </DialogTitle>
      <DialogContent>
        <form>
          <FormControl style={{ margin: 4, minWidth: 120 }}>
            <InputLabel htmlFor="user-email">
              <FormattedMessage id="app.login.email" />
            </InputLabel>
            <Input
              id="user-email"
              label="email"
              value={email}
              onChange={(e) => handleChange('email', e)}
            />
          </FormControl>
          <FormControl style={{ margin: 4, minWidth: 120 }}></FormControl>
          <FormControl style={{ margin: 4, minWidth: 120 }}>
            <InputLabel htmlFor="user-nickname">
              <FormattedMessage id="app.login.nickname" />
            </InputLabel>
            <Input
              id="user-nickname"
              label="nickname"
              value={nickname}
              onChange={(e) => handleChange('nickname', e)}
            />
          </FormControl>
          <FormControl style={{ margin: 4, minWidth: 120 }}>
            <InputLabel htmlFor="user-password">
              <FormattedMessage id="app.login.password" />
            </InputLabel>
            <Input
              id="user-password"
              label="password"
              type={showPassword ? 'text' : 'password'}
              value={password}
              onChange={(e) => handleChange('password', e)}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={changeShowPassword}
                  >
                    {!showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
        </form>
        <br />
        <p style={{ color: 'red' }}>{feedbackLogin}</p>
      </DialogContent>
      <DialogActions>
        {/* {this.state.registerFeedback} */}
        <Button onClick={close} color="primary">
          <FormattedMessage id="app.common.cancel" />
        </Button>
        <Button onClick={submit} color="primary">
          <FormattedMessage id="app.common.submit" />
        </Button>
      </DialogActions>
    </Dialog>
  );
};
