import React from 'react';

export const ChatMessage = ({ date, author, value }) =>
// @TODO: better handle the return
  (
    <li className="message">
      <span className="messageTime">({date})</span>
      {' '}
      <span className="messageAuthor">
        {author}
        :
      </span>
      <br />
      <span className="messageContent">{value}</span>
    </li>
  );
