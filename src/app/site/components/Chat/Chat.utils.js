const endpoint = 'https://abal.online/api/';

export const fetchMessages = async () => {
  const response = await fetch(`${endpoint}message/list`, {
    /* signal: this.abortController.signal */
  });
  const data = await response.json();

  if (response.status !== 200) {
    throw Error(body.message);
  }

  return data;
};

export const callBackendAPI = async () => {
  let cancel = false;
  const result = {};

  result.doCancel = () => { cancel = true; };

  result.data = await fetchMessages();

  if (cancel) {
    throw Error("Qu'Hansel");
  }

  return result;
};
