import React from 'react';
import { ChatMessage } from './ChatMessage';

export const ChatMessages = ({ messages }) => (
  <ul className="messagesList">
    {(messages || ['Chat error']).map((message, index) => (
      <ChatMessage
        key={`message${index}`}
        date={message.date}
        author={message.nickname}
        value={message.value}
      />
    ))}
  </ul>
);
