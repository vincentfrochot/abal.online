import './chat.scss';

import React, { useContext, useState, useEffect, useRef } from 'react';
import { Card } from '@material-ui/core';
import { callBackendAPI } from './Chat.utils';
import { ChatInput } from './ChatInput';
import { ChatMessages } from './ChatMessages';
import UserContext from '../../../contexts/UserContext';
import ChatContext from '../../../contexts/ChatContext';
import { SOCKET_EVENTS, SocketContext } from '../../../contexts/SocketContext';

// @TODO: Understand which architecture we want, to avoid each message to be rendered again when a new message arrives. We need to render only the new message.
export const Chat = () => {
  // SmartComponent : manage the logic, the state, etc. Send data to his DumbComponents.
  const messagesListRef = useRef(null);
  const userContext = useContext(UserContext);
  // const chatContext = useContext(ChatContext);
  const socketContext = useContext(SocketContext);

  const [messages, setMessages] = useState([]);

  if (messagesListRef.current) {
    messagesListRef.current.scrollTop = messagesListRef.current.scrollHeight;
  }
  // socketContext.socket.on(SOCKET_EVENTS.server.chat.message_pushed, (data) => {
  //   setMessages((messages) => [...messages, data.content.message]);
  // });

  useEffect(() => {
    socketContext.socket.emit(SOCKET_EVENTS.client.chat.chat_init);

    socketContext.socket.on(SOCKET_EVENTS.server.chat.message_pushed,
      handleIncomingMessage,
    );
    socketContext.socket.on(SOCKET_EVENTS.server.chat.chat_init, handleChatInitialization);

    socketContext.socket.on("reset_chat", () => {
      setMessages([]);
    });

    return function cleanup() {
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.chat.message_pushed,
        handleIncomingMessage,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.chat.chat_init,
        handleChatInitialization,
      );
      socketContext.socket.removeListener(
        "reset_chat",
        handleChatInitialization,
      );
    };
  }, []);

  const sendMessage = (message) => {
    socketContext.socket.emit('message_push', { value: message });
  };

  const handleIncomingMessage = (data) => {
    setMessages((messages) => [...messages, data.content.message]);

    if (messagesListRef.current) {
      messagesListRef.current.scrollTop = messagesListRef.current.scrollHeight;
    }
  };

  const handleChatInitialization = (data) => {
    setMessages(() => [...data.content.messages]);
    messagesListRef.current.scrollTop = messagesListRef.current.scrollHeight;
  };

  return (
    <div>
      <Card className="messagesContainer" ref={messagesListRef}>
        <ChatMessages messages={messages} />
      </Card>
      <Card className="messageContainer">
        <ChatInput onMessageSubmit={sendMessage} />
      </Card>
    </div>
  );
};
