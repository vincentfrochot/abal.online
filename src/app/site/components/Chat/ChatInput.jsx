import React, { useContext, useState, useEffect } from 'react';
import { Input, Button } from '@material-ui/core';

export const ChatInput = ({ onMessageSubmit }) => {
  const [messageBuffer, setMessageBuffer] = useState('');

  const handleInputMessageChange = (event) => {
    if (event.key === 'Enter') {
      if (messageBuffer !== '') {
        sendChatMessage();
      }
    } else {
      const message = event.target.value.substring(0, 380); // like a Tweet + 100
      setMessageBuffer(message);
    }
  };

  const sendChatMessage = () => {
    if (messageBuffer !== '') {
      onMessageSubmit(messageBuffer);
      setMessageBuffer('');
    }
  };

  return (
    <>
      <Input
        type="text"
        value={messageBuffer}
        onChange={handleInputMessageChange}
        onKeyPress={handleInputMessageChange}
      />
      <Button onClick={(e) => sendChatMessage()}>Send</Button>
    </>
  );
};
