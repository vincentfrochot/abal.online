import './login.scss';

import React, { useContext, useState, useEffect, useRef } from 'react';
import {
  Button,
  Input,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  InputLabel,
} from '@material-ui/core';
import UserContext from '../../../contexts/UserContext';
import { ConnectionDialog } from '../ConnectionDialog/ConnectionDialog';
import axios from 'axios';
import { SocketContext, SOCKET_EVENTS } from '../../../contexts/SocketContext';
import { FormattedMessage } from 'react-intl';

export const Login = () => {
  const { userContext, setAuthenticated, setNickname, setACL } = useContext(
    UserContext,
  );
  const socketContext = useContext(SocketContext);
  const [dialogIsOpen, openDialog] = useState(false);
  const [feedbackLogin, setFeedbackLogin] = useState(null);

  const loginClickHandler = () => {
    if (userContext.authenticated) {
      socketContext.socket.emit(SOCKET_EVENTS.client.user.user_log_out);
      localStorage.clear();
      setAuthenticated(false);
      return;
    }
    openDialog(true);
  };

  const handleInputChange = (e) => {
    setInputNickname(e.target.value);
  };

  const closeDialog = () => {
    openDialog(false);
  };

  const submitLogin = (data) => {
    if (data.email && data.nickname && data.password) {
      postData(data);
    } else {
      console.log("missing values");
    }
  };

  const postData = async (data) => {
    await axios
      .post(process.env.API_URL + '/api/v1/user/login', data)
      .then((response) => {
        const nickname = response.data.content.nickname;
        const acl = response.data.content.acl;
        if (nickname) {
          setAuthenticated(true);
          setFeedbackLogin('');
          setNickname(nickname);
          if (acl) {
            localStorage.setItem('acl', JSON.stringify(acl));
            setACL(JSON.stringify(acl));
          }
          localStorage.setItem('authenticated', true);
          localStorage.setItem('nickname', nickname);
          socketContext.socket.emit('user_log_in', {
            // device:,
            nickname,
            // language:,
          });
        }
        closeDialog();
      })
      .catch(function (error) {
        if (error.response) {
          if (error.response.data) {
            setFeedbackLogin(error.response.data.error);
          }
        } else {
          setFeedbackLogin('');
        }
      });
  };

  return (
    <>
      <ConnectionDialog
        open={dialogIsOpen}
        submitFunction={submitLogin}
        closeFunction={closeDialog}
        feedbackLogin={feedbackLogin}
        setFeedbackLogin={setFeedbackLogin}
      />
      {/* {!nickname && <Input id="header__login-input" value={inputNickname} onChange={handleInputChange} />}
      {nickname && (
      <Typography>
        Logged in as
        {nickname}
      </Typography>
      )} */}
      <Button id="header__login-button" onClick={loginClickHandler}>
        {userContext.authenticated ? (
          <FormattedMessage id="app.header--disconnect-button" />
        ) : (
          <FormattedMessage id="app.header--connect-button" />
        )}
      </Button>
    </>
  );
};
