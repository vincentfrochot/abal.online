import React from 'react';

const UserContext = React.createContext({
  userContext: {
    authenticated: false,
    nickname: '',
    isMobile: false,
    acl: {
      games: 'read',
      chat: 'write',
      // @TODO: generate default ACL here, and receive ACL of user when auth is done.
    },
  },
  setAuthenticated: () => {},
  setNickname: () => {},
  setACL: () => {},
});

export default UserContext;
