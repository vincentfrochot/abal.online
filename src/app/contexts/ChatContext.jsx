import React from 'react';

const ChatContext = React.createContext({
  messages: [],
});

export default ChatContext;
