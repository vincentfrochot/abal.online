import React from 'react';

const OnlineUserListContext = React.createContext({
  users: [],
});

export default OnlineUserListContext;
