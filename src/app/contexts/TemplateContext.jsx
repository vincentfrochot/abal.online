import React from 'react';

const TemplateContext = React.createContext({
  templateGamesColContext: '6',
  hdrContext: true,
  soundsContext: {
    game_created: false,
    rejoin_game: true,
    game_starts: true,
    move: false,
  },
  setTemplateGamesColContext: () => {},
  setHdrContext: () => {},
  setSoundsContext: () => {},
});

export default TemplateContext;
