import React from 'react';

const OnlineMatchListContext = React.createContext({
  matches: [],
});

export default OnlineMatchListContext;
