import React from 'react';

const GamesContext = React.createContext({
  gamesContext: {
    games: []
  },
  setGames: () => {},
});

export default GamesContext;
