import React from 'react';

export const SocketContext = React.createContext({
  socket: null
});

export const SOCKET_EVENTS = {
  client: {
    chat: {
      chat_init: "chat_init",
      message_push: "message_push", // { message }
    },
    user: {
      users_init: "users_init",
      user_log_in: "user_log_in", // at this point, call to API was sucessful and data was retrieved : { nickname, experience }
      user_log_out: "user_log_out", // { }
    },
    match: {
      matches_init: "matches_init",
    },
    game: {
      games_init: "games_init",
      game_create: "game_create", // { scores[], time, variant, ownerStarts... }
      game_analyze: "game_analyze",
      game_challenge: "game_challenge", // { gameId }
      game_cancel_challenge: "game_cancel_challenge",
      game_watch: "game_watch",
      game_start: "game_start", // { playerId }
      game_move_play: "game_move_play", // { gameId, move: Move }
      game_move_undo: "game_move_undo",
      game_resign: "game_resign", // { gameId }
      game_win_time: "game_win_time",
      game_quit: "game_quit",
    },
  },
  server: {
    chat: {
      chat_init: "chat_init",
      message_pushed: "message_pushed", // broadcast { nickname, message, time }
    },
    user: {
      number_of_users: "number_of_users",
      users_init: "users_init",
      user_joined: "user_joined",
      user_joined_you_users: "user_joined_you_users",
      user_joined_you_matches: "user_joined_you_matches",
      user_logged_in_users: "user_logged_in_users", // broadcast { users[] }
      user_logged_in_games: "user_logged_in_games",
      user_logged_in_you: "user_logged_in_you", // socket { acl,  }
      user_logged_out_users: "user_logged_out_users", // broadcast { users[] }
      user_logged_out_matches: "user_logged_out_matches",
      user_logged_out_you: "user_logged_out_you", // give back the user a "guest-WXYZ" nickname
      user_disconnected_users: "user_disconnected_users",
      user_disconnected_matches: "user_disconnected_matches",
    },
    match: {
      // list of matchs being played
      matches_init: "matches_init",
      match_created: "match_created", // broadcast
      match_started_match: "match_started_match",
      match_started_users: "match_started_users",
      match_move_played: "match_move_played",
      match_won_match: "match_won_match",
      match_won_users: "match_won_users",
      match_quited: "match_quited", // broadcast
      match_unwatch: "match_unwatch",
    },
    game: {
      // real games to display
      games_init: "games_init",
      game_created: "game_created", // socket { scores[], time, variant, ownerStarts... }
      game_created_sound: "game_created_sound",
      online_game_created: "online_game_created", // broadcast { onlineGames[] }
      game_challenged: "game_challenged", // broadcast { onlineGames }, notify each player linked to the game { playerId added to myGames  } => need to keep the association "gameId => playerId"  : socket.join(`games-${gameId}`): keep these in the class ???
      game_challenged_you: "game_challenged_you",
      game_challenged_owner: "game_challenged_owner",
      game_new_challenger_sound: "game_new_challenger_sound",
      game_canceled_challenge: "game_canceled_challenge",
      game_canceled_challenge_owner: "game_canceled_challenge_owner",
      game_canceled_challenge_you: "game_canceled_challenge_you",
      game_watched: "game_watched",
      // a game could have a "socket room ID" as property to easily "broadcast to people concerned".
      game_started: "game_started", // { playerId }
      game_started_sound: "game_started_sound",
      game_move_played: "game_move_played", // to each user associated to the game, { timer, boardHistory }
      game_move_played_sound: "game_move_played_sound",
      game_move_undone: "game_move_undone",
      game_won: "game_won",
      game_quited: "game_quited",
      games_to_end: "games_to_end",
      game_doesnt_exist: "game_doesnt_exist" // { gameId }
      // game_ended: "game_ended" { reason: string }
    },
  },
};