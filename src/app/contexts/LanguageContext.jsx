import React from 'react';

const LanguageContext = React.createContext({
  languageContext: 'en',
  setLanguageContext: () => {},
});

export default LanguageContext;
