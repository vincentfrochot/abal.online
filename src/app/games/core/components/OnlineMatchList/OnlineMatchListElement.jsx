import React from 'react';
import { TableRow, TableCell, Typography, Button } from '@material-ui/core';

import './onlineMatchList.scss';
import { useContext } from 'react';
import UserContext from '../../../../contexts/UserContext';
import {
  SocketContext,
  SOCKET_EVENTS,
} from '../../../../contexts/SocketContext';
import GamesContext from '../../../../contexts/GamesContext';
import { FormattedMessage } from 'react-intl';

export const OnlineMatchListElement = ({ match, pending }) => {
  const userContext = useContext(UserContext);
  const { gamesContext } = useContext(GamesContext);

  const socketContext = useContext(SocketContext);
  // our matches => green
  // match is pending => blue
  // match is playing => red
  // local analysis is grey

  let displayButton = true;
  let actionButton = 'play';
  let disablePlayButton = false;

  // disable "PLAY" action button if user is not authenticated. (Guests can watch).
  if (!userContext.userContext.authenticated) {
    disablePlayButton = true;
  }

  let gameState = 'pending';
  if (pending) {
    gameState = 'challenged';
    actionButton = 'cancel';
  } else if (match.started) {
    actionButton = 'watch';
    if (match.finished) {
      gameState = 'finished';
    } else {
      gameState = 'playing';
    }
  }

  if (match.type === 'analysis') {
    gameState = 'analysis';
  }

  // do not show any button on games you are already into
  const amIIn = gamesContext.games.find((game) => game.id === match.id);
  if (amIIn) {
    displayButton = false;
    gameState = 'my-game';
  }

  const handlePlay = () => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_challenge, {
      id: match.id,
    });
  };

  const handleWatch = () => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_watch, {
      id: match.id,
    });
  };

  const handleCancel = () => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_cancel_challenge, {
      gameId: match.id,
    });
  };

  // const pending = match.started && !match.finished;
  let className = `online-match-list-element__${gameState}`;

  let nicknameP1Display = match.nicknameP1;
  if (match.winner && match.winner === match.nicknameP1) {
    nicknameP1Display = <b style={{ color: 'green' }}>{match.nicknameP1}</b>;
  }
  let nicknameP2Display = match.nicknameP2;
  if (match.winner && match.winner === match.nicknameP2) {
    nicknameP2Display = <b style={{ color: 'green' }}>{match.nicknameP2}</b>;
  }

  return (
    <TableRow className={className}>
      {match.type !== 'analysis' ? (
        <>
          <TableCell className="min-padding online-match-list__action">
            {displayButton && actionButton === 'play' && (
              <Button
                onClick={handlePlay}
                className="online-match-list__play-button"
                disabled={disablePlayButton}
              >
                <FormattedMessage id="app.page.home.matches--play-button" />
              </Button>
            )}
            {displayButton && actionButton === 'watch' && (
              <Button
                onClick={handleWatch}
                className={"online-match-list__watch-button" + (match.finished && "-finished")}
              >
                {match.finished ? <FormattedMessage id="app.page.home.matches--review-button" /> : <FormattedMessage id="app.page.home.matches--watch-button" />}
              </Button>
            )}
            {displayButton && actionButton === 'cancel' && (
              <Button
                onClick={handleCancel}
                className="online-match-list__play-button"
              >
                <FormattedMessage id="app.page.home.matches--cancel-button" />
              </Button>
            )}
          </TableCell>
          <TableCell className={'min-padding'}>
            {match.scoreP1} {nicknameP1Display ? <b>{nicknameP1Display}</b> : <FormattedMessage id="app.page.home.matches--pending" />} <br />
            {match.scoreP2} {nicknameP2Display ? <span style={{"color":"white"}}><b>{nicknameP2Display}</b></span> : <FormattedMessage id="app.page.home.matches--pending" />}
          </TableCell>
          <TableCell className={'min-padding'}>
            {isNaN(match.startClockP1) ? '-' : match.startClockP1} <br />
            {isNaN(match.startClockP2) ? '-' : match.startClockP2}
          </TableCell>
          <TableCell className="min-padding match-list__xp match-list__playing">
            {match.variantName}
          </TableCell>
        </>
      ) : (
        <>
          <TableCell className="min-padding online-match-list__action">
            Analysis
          </TableCell>
          <TableCell>{match.nicknameP1 ?? match.nicknameP2}</TableCell>
          <TableCell>-</TableCell>
          <TableCell>-</TableCell>
        </>
      )}
    </TableRow>
  );
};
