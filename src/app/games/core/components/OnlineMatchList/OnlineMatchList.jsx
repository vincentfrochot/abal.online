import './onlineMatchList.scss';

import React, { useEffect, useState } from 'react';
import {
  Card,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core';
import { useContext } from 'react';
import { OnlineMatchListElement } from './OnlineMatchListElement';
import OnlineMatchListContext from '../../../../contexts/OnlineMatchListContext';
import {
  SOCKET_EVENTS,
  SocketContext,
} from '../../../../contexts/SocketContext';

export const OnlineMatchList = () => {
  const [onlineGames, setOnlineGames] = useState([]); // list of all existing games
  const [myPendingGames, setMyPendingGames] = useState([]);
  const socketContext = useContext(SocketContext);

  useEffect(() => {
    socketContext.socket.emit(SOCKET_EVENTS.client.match.matches_init);

    socketContext.socket.on(
      SOCKET_EVENTS.client.match.matches_init,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_challenged_you,
      handlePendingAsChallenger,
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_canceled_challenge_you,
      handleCancelChallenge,
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_joined_you_matches,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_logged_out_matches,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_disconnected_matches,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_created,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_started_match,
      (data) => {
        const match = data.content.match;
        if (match) {
          setOnlineGames((previous) => {
            const existingMatch = previous.find((e) => match.id === e.id);
            if (existingMatch !== undefined) {
              return previous.map((element) => {
                return element.id === existingMatch.id ? match : element;
              });
            }
            return [data.match, ...previous];
          });
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_move_played,
      (data) => {
        const game = data.content.match;
        setOnlineGames((previous) => {
          const existingGame = previous.find((e) => game.id === e.id);
          if (existingGame !== undefined) {
            return previous.map((element) => {
              return element.id === game.id ? game : element;
            });
          } else {
            return [...previous, game];
          }
        });
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_won_match,
      (data) => {
        const match = data.content.match;
        setOnlineGames((previous) => {
          const existingMatch = previous.find((e) => match.id === e.id);
          if (existingMatch !== undefined) {
            return [...previous.filter((e) => e.id !== match.id), match];
            // return previous.map((element) => { // => was not working
            //   return element.id === match.id ? existingMatch : element;
            // });
          }
        });
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_unwatch,
      (data) => {
        setOnlineGames(data.content.matches);
      },
    );
    // remove match from list
    socketContext.socket.on(SOCKET_EVENTS.server.match.match_quited, (data) => {
      setOnlineGames((matches) =>
        matches.filter((e) => e.id !== data.content.id),
      );
    });

    return function cleanup() {
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_challenged_you,
        handlePendingAsChallenger,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_canceled_challenge_you,
        handleCancelChallenge,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_joined_you_matches,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_logged_out_matches,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_disconnected_matches,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_created,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_started_match,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_move_played,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_won_match,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_unwatch,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_quited,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.matches_init,
      );
    };
  }, []);

  const handlePendingAsChallenger = (data) => {
    if (data.content) {
      setMyPendingGames((previous) => {
        return [...previous, data.content.gameId];
      });
    }
  };

  const handleCancelChallenge = (data) => {
    if (data.content) {
      // @TODO: remove received id from array
      const gameId = '' + data.content.gameId;
      setMyPendingGames((previous) => {
        const existingGame = previous.find((id) => '' + id === '' + gameId);
        if (existingGame) {
          return [...previous.filter((id) => '' + id !== '' + gameId)];
        } else {
          return [...previous];
        }
      });
    }
  };

  return (
    <Card id="online-match-list">
      {/* <Typography variant="h6" id="tableTitle">
        Players
      </Typography> */}
      <Table size="small">
        {/* <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell className="players-list__xp">XP</TableCell>
          </TableRow>
        </TableHead> */}
        <TableBody>
          {onlineGames.map((match) => (
            <OnlineMatchListElement
              match={match}
              key={'match-' + match.finished + '-' + match.id}
              pending={
                !match.started &&
                myPendingGames.findIndex((e) => e === match.id) !== -1
              }
            />
          ))}
        </TableBody>
      </Table>
    </Card>
  );
};
