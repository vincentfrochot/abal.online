import React, { useEffect } from 'react';
import { useState } from 'react';

export const Timer = ({
  x,
  y,
  value,
  turn,
  started,
  finished,
  refTime,
  type,
  outOfTimeHandler
}) => {
  // @TODO: use optimistic approach : update client UI before receiving data from Server, then synchronize. For example, we need to freeze the timer of current player turn before everybody receive confirmation (that he played) from the server.
  const [time, setTime] = useState(0);
  const refTimeDate = new Date(refTime).getTime();

  useEffect(() => {
    let interval;
    if (!finished && started && turn) {
      interval = setInterval(() => {
        const timeNow = new Date();
        const timeDiff = timeNow.getTime() - refTimeDate;
        const secsDiff = Math.floor(timeDiff / 1000);
        setTime(secsDiff);
      }, 1000); // date reference - date.now() => évite d'avoir trop de désynchro cumulée
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [turn, started, refTime]);

  const displayTime = (time) => {
    let minutes = 0,
      seconds = 0;
    if (!isNaN(time)) {
      minutes = Math.floor((time / 60).toString());
      seconds = (time % 60).toString();
      if (minutes.length < 2) {
        minutes = '0' + minutes;
      }
      if (seconds.length < 2) {
        seconds = '0' + seconds;
      }
    }
    return { minutes, seconds };
  };

  let timeToDisplay;
  if (type === 'analysis') {
    timeToDisplay = displayTime(time);
  } else {
    timeToDisplay = displayTime(value - time);
    if (timeToDisplay.minutes < 0) {
      outOfTimeHandler();
    }
  }

  return (
    <>
      <text x={x} y={y}>
        {timeToDisplay.minutes < 0
          ? '-'
          : timeToDisplay.minutes + ':' + timeToDisplay.seconds}
      </text>
    </>
  );
};
