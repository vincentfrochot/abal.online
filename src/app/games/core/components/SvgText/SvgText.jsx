import React from 'react';

export const SvgText = (props) => {
  const {
    x, y, fontSize, fill, label
  } = props;

  return (
    <text
      x={x}
      y={y}
      fill="#414141"
      fontFamily={'DejaVu Serif'}
      fontSize={fontSize}
      // className={this.className}
      fill={fill}
      // transform={this.transform}
    >
      {label}
    </text>
  );
};
