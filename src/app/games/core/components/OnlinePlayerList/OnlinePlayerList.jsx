import React, { useEffect, useState } from 'react';
import {
  Card,
  Table,
  TableBody,
} from '@material-ui/core';
import { useContext } from 'react';
import { OnlinePlayerListElement } from './OnlinePlayerListElement';

import './onlinePlayerList.scss';
import {
  SOCKET_EVENTS,
  SocketContext,
} from '../../../../contexts/SocketContext';

export const OnlinePlayerList = () => {
  const [onlineUsers, setOnlineUsers] = useState([]); // list of all connected users

  const socketContext = useContext(SocketContext);

  useEffect(() => {
    socketContext.socket.emit(SOCKET_EVENTS.client.user.users_init);

    socketContext.socket.on(
      SOCKET_EVENTS.server.user.users_init,
      handleInit
    );
    socketContext.socket.on(SOCKET_EVENTS.server.user.user_joined, (data) => {
      setOnlineUsers(data.content.users);
    });
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_logged_in_users,
      (data) => {
        if (data.content.users) {
          setOnlineUsers(data.content.users);
        }
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_logged_out_users,
      (data) => {
        setOnlineUsers(data.content.users);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.user.user_disconnected_users,
      (data) => {
        setOnlineUsers(data.content.users);
      },
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.match.match_started_users,
      (data) => {
        setOnlineUsers((previous) => {
          return data.content.users;
        });
      },
    );
    socketContext.socket.on(SOCKET_EVENTS.server.match.match_won_users, (data) => {
      if (data.content.users) {
        setOnlineUsers(data.content.users);
      }
    });

    return () => {
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.users_init,
        handleInit
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_joined,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_logged_in_users,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_logged_out_users,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.user.user_disconnected_users,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_started_users,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.match.match_won_users,
      );
    };
  }, []);

  const handleInit = (data) => {
    setOnlineUsers(() => [...data.content.users]);
  };

  // console.log(onlineUsersContext); // @TODO: recevoir "authenticated" dans la structure User renvoyée par le server !!

  return (
    <Card id="online-player-list">
      {/* <Typography variant="h6" id="tableTitle">
        Players
      </Typography> */}
      <Table size="small">
        {/* <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell className="players-list__xp">XP</TableCell>
          </TableRow>
        </TableHead> */}
        <TableBody>
          {onlineUsers.map((user) => (
            <OnlinePlayerListElement
              key={
                user.socketId + '-' + user.authenticated + '-' + user.experience
              }
              user={user}
              authenticated={user.authenticated}
              isMe={user.socketId === socketContext.socket.id}
            />
          ))}
        </TableBody>
      </Table>
    </Card>
  );
};
