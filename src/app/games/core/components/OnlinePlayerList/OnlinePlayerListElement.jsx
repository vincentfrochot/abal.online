import React from 'react';
import { TableRow, TableCell, Typography } from '@material-ui/core';
import LaptopIcon from '../../../../../assets/icons/LaptopIcon';
import MobileIcon from '../../../../../assets/icons/MobileIcon';

export const OnlinePlayerListElement = ({ user, authenticated, isMe }) => {

  // @TODO: THE SERVER MUST SEND THE FOLLOWING INFORMATION: IS THE PLAYER CURRENTLY PLAYING ?
  // So we can change the TableRow className depending on this data.
  // yellow => player has a game pending

  // player is playing => red
  // player is playing and has a game pending => orange
  // player has a game pending => yellow
  // player is available => blue
  // not authenticated => white
  // ourself => green

  // could be nice to add a little "level indicator" as an icon instead of the device type (mobile or desktop)
  // white belt, ... red belt, black belt, master, world champion, current world champion

  let userState = 'available';
  if (isMe) {
    userState = 'ourself';
  } else if (!authenticated) {
    userState = 'unauthenticated';
  } else if (user.playing) {
    userState = 'playing';
  }

  let className = `online-player-list__${userState}`;

  return (
    <TableRow className={className}>
      <TableCell className={"min-padding"}>
        {' '}
        {user.device === 'mobile' ? <MobileIcon /> : <LaptopIcon />}{' '}
        {user.nickname} {user.ping > 0 ? "(" + user.ping + ")" : ''}
      </TableCell>
      <TableCell className="min-padding online-player-list__xp">{user.experience ?? "-"}</TableCell>
    </TableRow>
  );
};
