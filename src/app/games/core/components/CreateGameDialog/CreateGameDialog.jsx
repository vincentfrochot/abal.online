import React, { useContext, useState, useEffect, useRef } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  FormControl,
  InputLabel,
  Select,
  DialogActions,
  Button,
  FormGroup,
  FormControlLabel,
  Switch,
  TextField,
  Input,
} from '@material-ui/core';
import { variantToMap } from '../../../abalone/components/Game/game.utils';
import { FormattedMessage } from 'react-intl';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

export const CreateGameDialog = ({ open, closeFunction, submitFunction }) => {
  const [type, setType] = useState('match');
  const [player1, togglePlayer1] = useState(false);
  const [variant, setVariant] = useState('belgian_daisy');
  const [score, setScore] = useState([0, 0]);
  const [clock, setClock] = useState(['15', '15']);
  const [displayInputCustomVariant, setDisplayInputCustomVariant] = useState(
    false,
  );
  const [customVariant, setCustomVariant] = useState('');

  const handleGameCreation = (changedParameter, e) => {
    if (changedParameter === 'gameSetupType') {
      setType(e.target.value);
    } else if (changedParameter === 'gameSetupPlayer1') {
      togglePlayer1((previous) => !previous);
    } else if (changedParameter === 'gameSetupVariant') {
      const variant = e.target.value;
      if (variant === 'custom') {
        setDisplayInputCustomVariant(true);
      } else {
        setDisplayInputCustomVariant(false);
      }
      setVariant(variant);
    } else if (changedParameter === 'gameSetupCustomVariant') {
      setCustomVariant(e.target.value);
    } else if (changedParameter === 'gameSetupScorePlayer1') {
      const val = e.target.value;
      setScore((previous) => [val, previous[1]]);
    } else if (changedParameter === 'gameSetupScorePlayer2') {
      const val = e.target.value;
      setScore((previous) => [previous[0], val]);
    } else if (changedParameter === 'gameSetupClockPlayer1') {
      const val = e.target.value;
      setClock((previous) => [val, previous[1]]);
    } else if (changedParameter === 'gameSetupClockPlayer2') {
      const val = e.target.value;
      setClock((previous) => [previous[0], val]);
    }
  };

  const submit = () => {
    let boardPosition;
    if (variant === 'custom') {
      const boardPositionTmp = customVariant.split(',') || [];
      boardPosition = boardPositionTmp.map((e) => parseInt(e));
    } else {
      boardPosition = variantToMap[variant];
    }
    submitFunction({
      type,
      player1,
      variant: {
        name: variant,
        boardPosition: boardPosition,
      },
      scoreP1: score[0],
      scoreP2: score[1],
      clockP1: clock[0],
      clockP2: clock[1],
    });
    closeFunction();
  };

  return (
    <Dialog
      open={open}
      // onClose={closeModal}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">
        <FormattedMessage id="app.page.home.create-game-modale--title" />
      </DialogTitle>
      <DialogContent>
        <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="score-setup">
            <FormattedMessage id="app.page.home.create-game-modale--game-type" />
          </InputLabel>
          <Select
            native
            value={type}
            onChange={(e) => handleGameCreation('gameSetupType', e)}
            inputProps={{
              id: 'type-setup',
            }}
          >
            <option value="match">match</option>
            <option value="analysis">analyse</option>
            <option value="match" disabled>
              correspondence
            </option>
          </Select>
        </FormControl>

        <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="edit-mode-setup">
            <FormattedMessage id="app.page.home.create-game-modale--edit-mode" />
          </InputLabel>
          <Select
            disabled
            native
            // value={this.state.gameSetupEditMode}
            // onChange={this.handleGameCreation('gameSetupEditMode')}
            inputProps={{
              id: 'edit-mode-setup',
            }}
          >
            <option value={1}>disabled</option>
          </Select>
        </FormControl>

        <br />

        <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="score1-setup">
            <FormattedMessage id="app.page.home.create-game-modale--score-p1" />
          </InputLabel>
          <Select
            native
            value={score[0]}
            onChange={(e) => handleGameCreation('gameSetupScorePlayer1', e)}
            inputProps={{
              id: 'score1-setup',
            }}
          >
            <option value={0}>0</option>
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
            <option value={4}>4</option>
            <option value={5}>5</option>
          </Select>
        </FormControl>
        <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="score2-setup">
            <FormattedMessage id="app.page.home.create-game-modale--score-p2" />
          </InputLabel>
          <Select
            native
            value={score[1]}
            onChange={(e) => handleGameCreation('gameSetupScorePlayer2', e)}
            inputProps={{
              id: 'score2-setup',
            }}
          >
            <option value={0}>0</option>
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
            <option value={4}>4</option>
            <option value={5}>5</option>
          </Select>
        </FormControl>

        <br />

        <FormControl style={{ margin: 4, maxWidth: 120 }}>
          <InputLabel htmlFor="clock1-setup">
            <FormattedMessage id="app.page.home.create-game-modale--clock-p1" />
          </InputLabel>
          <Select
            native
            inputProps={{
              id: 'clock1-setup',
            }}
            label="Clock P1"
            value={clock[0]}
            onChange={(e) => handleGameCreation('gameSetupClockPlayer1', e)}
          >
            <option value={'2.30'}>2.30</option>
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={15}>15</option>
            <option value={30}>30</option>
            <option value={60}>60</option>
            <option value={' '}>No limit</option>
          </Select>
        </FormControl>
        <FormControl style={{ margin: 4, maxWidth: 120 }}>
          <InputLabel htmlFor="clock2-setup">
            <FormattedMessage id="app.page.home.create-game-modale--clock-p2" />
          </InputLabel>
          <Select
            native
            inputProps={{
              id: 'clock2-setup',
            }}
            label="Clock P2"
            value={clock[1]}
            onChange={(e) => handleGameCreation('gameSetupClockPlayer2', e)}
          >
            <option value={'2.30'}>2.30</option>
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={15}>15</option>
            <option value={30}>30</option>
            <option value={60}>60</option>
            <option value={' '}>No limit</option>
          </Select>
        </FormControl>
        {/* <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="timer2-setup">Time P2</InputLabel>
          <Select
            disabled
            native
            // value={this.state.gameSetupTimer2}
            // onChange={this.handleGameCreation('gameSetupScore')}
            inputProps={{
              id: 'timer2-setup',
            }}
          >
            <option value={1}>Eternal</option>
          </Select>
        </FormControl> */}

        <br />

        <FormControl style={{ margin: 4, minWidth: 120 }}>
          <InputLabel htmlFor="variant-setup">
            <FormattedMessage id="app.page.home.create-game-modale--variant" />
          </InputLabel>
          <Select
            native
            value={variant}
            onChange={(e) => handleGameCreation('gameSetupVariant', e)}
            inputProps={{
              id: 'variant-setup',
            }}
          >
            <option value="belgian_daisy">Belgian Daisy</option>
            <option value="the_clearing">The clearing</option>
            <option value="german_daisy">German Daisy</option>
            <option value="star">Star</option>
            {/* <option value="custom">Custom</option> */}
            <option value="dutch_daisy">Dutch Daisy</option>
            <option value="swiss_daisy">Swiss Daisy</option>
            <option value="snakes_variant">Snakes Variant</option>
            <option value="alien">Alien Attack</option>
            <option value="the_wall">The Wall</option>
            <option value="duel">Duel</option>
            <option value="centrifugeuse">Centrifugeuse</option>
            <option value="atomouche">Atomouche</option>
            <option value="corners">Corners</option>
            <option value="alliances">Alliances</option>
            <option value="domination">Domination</option>
            <option value="fujiyama">Fujiyama</option>
            <option value="standard">Standard</option>
            <option value="accelium">Clash (Accelium)</option>
          </Select>
        </FormControl>
        <FormGroup>
          {displayInputCustomVariant && (
            <input
              name="customVariant"
              onChange={(e) => handleGameCreation('gameSetupCustomVariant', e)}
              value={customVariant}
            />
          )}
        </FormGroup>
        <FormGroup row>
          {/* <Typography component="div">
        <Grid component="label" container alignItems="center" spacing={1}>
          <Grid item>Off</Grid>
          <Grid item>
            <AntSwitch checked={state.checkedC} onChange={handleChange} name="checkedC" />
          </Grid>
          <Grid item>On</Grid>
        </Grid>
      </Typography> */}
          <FormControlLabel
            control={
              <Switch
                checked={player1}
                onChange={(e) => handleGameCreation('gameSetupPlayer1', e)}
                value={player1}
                color="primary"
              />
            }
            label={
              player1 ? (
                <FormattedMessage id="app.page.home.create-game-modale--play-black" />
              ) : (
                <FormattedMessage id="app.page.home.create-game-modale--play-white" />
              )
            }
          />
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeFunction} color="primary">
          <FormattedMessage id="app.common.cancel" />
        </Button>
        <Button onClick={submit} color="primary">
          <FormattedMessage id="app.common.submit" />
        </Button>
      </DialogActions>
    </Dialog>
  );
};
