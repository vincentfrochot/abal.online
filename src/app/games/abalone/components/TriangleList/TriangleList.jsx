import React from 'react';
import { PointyTopTriangle } from '../Triangle/PointyTopTriangle';
import { PointyBottomTriangle } from '../Triangle/PointyBottomTriangle';
import { coordinates2D } from '../Square/square.utils';
import { trianglesMap } from '../Triangle/triangle.utils';
import { useContext } from 'react';
import TemplateContext from '../../../../contexts/TemplateContext';

export const TriangleList = React.memo(() => {
  const { hdrContext } = useContext(TemplateContext);

  const topTriangles = [...coordinates2D].map((position2D) => (
    <PointyTopTriangle
      key={`pointy-top-triangle-${position2D[0]}-${position2D[1]}`}
      position2D={position2D}
      hdr={hdrContext}
    />
  ));
  const bottomTriangles = coordinates2D.map((position2D) => (
    <PointyBottomTriangle
      key={`pointy-bottom-triangle-${position2D[0]}-${position2D[1]}`}
      position2D={position2D}
      hdr={hdrContext}
    />
  ));
  const externalTopTriangles = trianglesMap
    .filter((value) => value[1] === 'top' || value[1] === 'both')
    .map((value) => (
      <PointyTopTriangle
        key={`pointy-top-triangle-${value[0][0]}-${value[0][1]}`}
        position2D={value[0]}
        hdr={hdrContext}
      />
    ));
  const externalBottomTriangles = trianglesMap
    .filter((value) => value[1] === 'bottom' || value[1] === 'both')
    .map((value) => (
      <PointyBottomTriangle
        key={`pointy-bottom-triangle-${value[0][0]}-${value[0][1]}`}
        position2D={value[0]}
        hdr={hdrContext}
      />
    ));

  return (
    <>
      {topTriangles}
      {externalTopTriangles}
      {bottomTriangles}
      {externalBottomTriangles}
    </>
  );
});
