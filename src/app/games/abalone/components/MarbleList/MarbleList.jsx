import React, { useContext } from 'react';
import { Marble } from '../Marble/Marble';
import {
  squaresCoordinates,
  coordinates2D,
  ripSquaresCoordinates,
} from '../Square/square.utils';
import { marbleCoordinateToSquareCoordinate } from '../Game/game.utils';

import AbaloneThemeContext from '../../contexts/AbaloneThemeContext';

export const MarbleList = (props) => {
  const {
    positions,
    selected,
    clickHandler,
    vector,
    score,
    turn,
    myColor,
    ...other
  } = props;
  const boardContext = useContext(AbaloneThemeContext);

  const vectorDirection = vector?.direction || '';
  // const marblesCoordinates1D = vector?.coordinates1D.map((e) => e) || null;
  const marblesCoordinates1D = vector?.arrowPositions?.map((e) => e) || null;
  const blackColor = boardContext.marbles.color[0];
  const whiteColor = boardContext.marbles.color[1];
  const nothingHandler = () => {};

  if (!positions) {
    return <></>;
  }
  const blackMarbles = positions
    .filter((e) => e % 2 === 0)
    .map((pos) => {
      const squarePos = marbleCoordinateToSquareCoordinate(pos);
      return (
        <Marble
          key={`black-marble-${squarePos}`}
          moved={
            marblesCoordinates1D && marblesCoordinates1D.includes(pos)
              ? vectorDirection
              : 0
          }
          position={pos}
          player={0}
          color={blackColor}
          opponentColor={whiteColor}
          x={
            squaresCoordinates[coordinates2D[squarePos][0]][
              coordinates2D[squarePos][1]
            ].x
          }
          y={
            squaresCoordinates[coordinates2D[squarePos][0]][
              coordinates2D[squarePos][1]
            ].y
          }
          rip={false}
          selected={selected !== null && selected === pos}
          clickHandler={clickHandler}
        />
      );
    });

  const whiteMarbles = positions
    .filter((e) => e % 2 === 1)
    .map((pos) => {
      const squarePos = marbleCoordinateToSquareCoordinate(pos);
      return (
        <Marble
          key={`white-marble-${squarePos}`}
          moved={
            marblesCoordinates1D && marblesCoordinates1D.includes(pos)
              ? vectorDirection
              : 0
          }
          position={pos}
          player={1}
          color={whiteColor}
          opponentColor={blackColor}
          x={
            squaresCoordinates[coordinates2D[squarePos][0]][
              coordinates2D[squarePos][1]
            ].x
          }
          y={
            squaresCoordinates[coordinates2D[squarePos][0]][
              coordinates2D[squarePos][1]
            ].y
          }
          rip={false}
          selected={selected !== null && selected === pos}
          clickHandler={clickHandler}
        />
      );
    });

  const ripBlackMarbles = [];
  let scoreBlack = score[0];
  while (scoreBlack--) {
    ripBlackMarbles.push(
      <Marble
        key={`rip-black-marble-${scoreBlack}`}
        position={scoreBlack}
        player={1}
        color={whiteColor}
        opponentColor={blackColor}
        x={ripSquaresCoordinates[scoreBlack].x}
        y={ripSquaresCoordinates[scoreBlack].y}
        rip
        selected={false}
        clickHandler={nothingHandler}
      />,
    );
  }
  const ripWhiteMarbles = [];
  let scoreWhite = 0;
  while (scoreWhite++ < score[1]) {
    ripWhiteMarbles.push(
      <Marble
        key={`rip-white-marble-${scoreWhite}`}
        position={scoreWhite + 5}
        player={0}
        color={blackColor}
        opponentColor={whiteColor}
        x={ripSquaresCoordinates[scoreWhite + 5].x}
        y={ripSquaresCoordinates[scoreWhite + 5].y}
        rip
        selected={false}
        clickHandler={nothingHandler}
      />,
    );
  }

  const turnIndicator = (
    <Marble
      key={`turn-marble-${turn}`}
      position={666}
      player={turn}
      color={turn ? whiteColor : blackColor}
      opponentColor={turn ? blackColor : whiteColor}
      x={-30}
      y={turn ? 85 : 205}
      rip={true}
      selected={false}
      clickHandler={nothingHandler}
    />
  );

  return (
    <>
      {blackMarbles}
      {whiteMarbles}
      {ripBlackMarbles}
      {ripWhiteMarbles}
      {turnIndicator}
    </>
  );
};
