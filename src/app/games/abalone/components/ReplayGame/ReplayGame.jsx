import React from 'react';
import { Hexagon } from '../Hexagon/Hexagon';
import { SquareList } from '../SquareList/SquareList';
import { TriangleList } from '../TriangleList/TriangleList';
import { Board } from '../Board/Board';

export const ReplayGame = (props) => {
  const { displayedMoveIndex, game } = props;

  let marblesPositions,
    score,
    marblesVector = [],
    startTimerP2AsArray,
    startClockTimeP2InSecForMinutes,
    startClockTimeP2InSecForSec,
    remainingTimeP1SecAsString,
    remainingTimeP2SecAsString,
    remainingTimeP1InSec,
    remainingTimeP2InSec,
    remainingTimeP1AsString,
    remainingTimeP2AsString;
  if (game) {
    marblesPositions = game.histoPositions[displayedMoveIndex];
    score = game.histoScores[displayedMoveIndex];
    marblesVector = {
      direction: game.histoVectorsDirections[displayedMoveIndex],
      arrowPositions: game.histoVectorsArrowsPositions[displayedMoveIndex],
    };

    if (displayedMoveIndex === 0) {
      remainingTimeP1AsString = game.rules.timer.player[0];
      remainingTimeP2AsString = game.rules.timer.player[1];
    } else {
      if (displayedMoveIndex % 2) {
        if (displayedMoveIndex === 1) {
          remainingTimeP1InSec = game.histoClocks[displayedMoveIndex - 1]; // startClockTimeP1InSec - timeDiffP1InSec;
          remainingTimeP1SecAsString = Math.floor(remainingTimeP1InSec % 60);
          remainingTimeP1SecAsString =
            parseInt(remainingTimeP1SecAsString) < 10
              ? '0' + remainingTimeP1SecAsString
              : remainingTimeP1SecAsString;

          startTimerP2AsArray = game.rules.timer.player[1].split('.');
          startClockTimeP2InSecForMinutes =
            parseInt(startTimerP2AsArray[0]) * 60;
          startClockTimeP2InSecForSec = parseInt(startTimerP2AsArray[1] || 0);
          remainingTimeP2InSec =
            startClockTimeP2InSecForMinutes + startClockTimeP2InSecForSec;
          remainingTimeP2SecAsString = Math.floor(remainingTimeP2InSec % 60);
          remainingTimeP2SecAsString =
            parseInt(remainingTimeP2SecAsString) < 10
              ? '0' + remainingTimeP2SecAsString
              : remainingTimeP2SecAsString;
        } else {
          remainingTimeP1InSec = game.histoClocks[displayedMoveIndex - 1];
          remainingTimeP1SecAsString = Math.floor(remainingTimeP1InSec % 60);
          remainingTimeP1SecAsString =
            parseInt(remainingTimeP1SecAsString) < 10
              ? '0' + remainingTimeP1SecAsString
              : remainingTimeP1SecAsString;

          remainingTimeP2InSec = game.histoClocks[displayedMoveIndex - 2];
          remainingTimeP2SecAsString = Math.floor(remainingTimeP2InSec % 60);
          remainingTimeP2SecAsString =
            parseInt(remainingTimeP2SecAsString) < 10
              ? '0' + remainingTimeP2SecAsString
              : remainingTimeP2SecAsString;
        }
      } else {
        remainingTimeP1InSec = game.histoClocks[displayedMoveIndex - 2];
        remainingTimeP1SecAsString = Math.floor(remainingTimeP1InSec % 60);
        remainingTimeP1SecAsString =
          parseInt(remainingTimeP1SecAsString) < 10
            ? '0' + remainingTimeP1SecAsString
            : remainingTimeP1SecAsString;

        remainingTimeP2InSec = game.histoClocks[displayedMoveIndex - 1];
        remainingTimeP2SecAsString = Math.floor(remainingTimeP2InSec % 60);
        remainingTimeP2SecAsString =
          parseInt(remainingTimeP2SecAsString) < 10
            ? '0' + remainingTimeP2SecAsString
            : remainingTimeP2SecAsString;
      }
      remainingTimeP1AsString =
        Math.floor(remainingTimeP1InSec / 60) +
        '.' +
        remainingTimeP1SecAsString;
      remainingTimeP2AsString =
        Math.floor(remainingTimeP2InSec / 60) +
        '.' +
        remainingTimeP2SecAsString;
    }
  }

  const turn = displayedMoveIndex % 2;

  const squareClickHandler = (position) => {
    console.log(position);
  };

  const marbleClickHandler = () => {};

  return (
    <>
      <Hexagon gametype={'analysis'} finished={true} />
      <SquareList clickHandler={squareClickHandler} />
      <TriangleList />
      <Board
        key={game?._id || 'pending-load'}
        marbleClickHandler={marbleClickHandler}
        marblesPositions={marblesPositions}
        marblesVector={marblesVector}
        score={score}
        turn={turn}
      />

      {game?.players.nicknames && (
        <text x="-10" y="220" style={{ font: 'italic 6px sans-serif' }}>
          {game?.players.nicknames[0]}
        </text>
      )}
      {remainingTimeP1AsString !== 'NaN.NaN' && (
        <text x="10" y="230" style={{ font: 'italic 6px sans-serif' }}>
          {remainingTimeP1AsString}
        </text>
      )}

      {game?.players.nicknames && (
        <text x="-10" y="70" style={{ font: 'italic 6px sans-serif' }}>
          {game?.players.nicknames[1]}
        </text>
      )}
      {remainingTimeP2AsString !== 'NaN.NaN' && (
        <text x="10" y="80" style={{ font: 'italic 6px sans-serif' }}>
          {remainingTimeP2AsString}
        </text>
      )}
    </>
  );
};
