import React from 'react';
import style from './arrow.scss';

import {
  TOP_RIGHT,
  RIGHT,
  BOTTOM_RIGHT,
  TOP_LEFT,
  LEFT,
  BOTTOM_LEFT,
} from './arrow.utils';

export const Arrow = (props) => {
  const {
    radius, direction, color, x, y, className,
  } = props;
  const arrowRadius = radius * 0.55;
  const sin = 0.5 * arrowRadius;
  const cos = 0.86602540378 * arrowRadius;

  let path;

  if (direction === TOP_RIGHT) {
    path = `M${
      x - sin
    } ${
      y - cos
    }L${
      x + sin
    } ${
      y - cos
    }L${
      x + arrowRadius
    } ${
      y}`;
  }
  if (direction === RIGHT) {
    path = `M${
      x + sin
    },${
      y - cos
    }L${
      x + arrowRadius
    },${
      y
    }L${
      x + sin
    },${
      y + cos}`;
  }
  if (direction === BOTTOM_RIGHT) {
    path = `M${
      x + arrowRadius
    },${
      y
    }L${
      x + sin
    },${
      y + cos
    }L${
      x - sin
    },${
      y + cos}`;
  }
  if (direction === BOTTOM_LEFT) {
    path = `M${
      x + sin
    },${
      y + cos
    }L${
      x - sin
    },${
      y + cos
    }L${
      x - arrowRadius
    },${
      y}`;
  }
  if (direction === LEFT) {
    path = `M${
      x - sin
    },${
      y + cos
    }L${
      x - arrowRadius
    },${
      y
    }L${
      x - sin
    },${
      y - cos}`;
  }
  if (direction === TOP_LEFT) {
    path = `M${
      x - arrowRadius
    },${
      y
    }L${
      x - sin
    },${
      y - cos
    }L${
      x + sin
    },${
      y - cos}`;
  }

  return (
    <path
      d={path}
      fill="none"
      strokeWidth={2.1}
      className={className}
      strokeOpacity={0.9}
      strokeLinecap="round"
      strokeLinejoin="bevel"
    />
  );
};
