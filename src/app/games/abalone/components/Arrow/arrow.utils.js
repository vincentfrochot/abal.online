export const TOP_RIGHT = 1;
export const RIGHT = 2;
export const BOTTOM_RIGHT = 3;
export const BOTTOM_LEFT = 4;
export const LEFT = 5;
export const TOP_LEFT = 6;

export const directions = [
  '',
  'TOP_RIGHT',
  'RIGHT',
  'BOTTOM_RIGHT',
  'BOTTOM_LEFT',
  'LEFT',
  'TOP_LEFT',
];
