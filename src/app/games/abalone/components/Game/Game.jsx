import React, { useState, useEffect } from 'react';
import { Board } from '../Board/Board';
import { TriangleList } from '../TriangleList/TriangleList';
import { Hexagon } from '../Hexagon/Hexagon';
import { SquareList } from '../SquareList/SquareList';
import {
  computeMove,
  marbleCoordinateToSquareCoordinate,
  squareCoordinateToMarbleCoordinate,
  variantToMap,
} from './game.utils';
import { Timer } from '../../../core/components/Timer/Timer';
import { useContext } from 'react';
import { SocketContext } from '../../../../contexts/SocketContext';

export const Game = ({
  identifier, move, moveCallBack, endgameHandler, finished, started, variant, scoreP1, scoreP2, startClockP1, startClockP2, histoPositions, lastMoveNumber
}) => {
  // @TODO: if move is SET to a too high value, I need to prevent this and reset it to max possible value.
  const [maxMove, setMaxMove] = useState(0);
  const [selectedMarble, setSelectedMarble] = useState(null);
  const [turn, toggleTurn] = useState(false);

  const [scores, setScores] = useState([[scoreP1, scoreP2]]);
  const [history, setHistory] = useState([
    // 1D variant
    variantToMap[variant],
  ]);

  const [vectors, setVectors] = useState([{ direction: 0, coordinates1D: [] }]);
  const [targetPosition, setTargetPosition] = useState(null);

  const displayedMove = move > maxMove ? maxMove : move;

  // forced to use useEffect() instead of triggering the state change inside to avoid re-generating a new function each time for squareClickHandler.
  useEffect(() => {
    // remember useEffect is to tell React to call this function AFTER targetPosition was changed and the DOM rendered.
    handleMove();
  }, [targetPosition]);

  useEffect(() => {
    moveCallBack(maxMove);
  }, [maxMove]);

/*  
  useEffect(() => {
    console.log("histo changed");
    console.log(histoPositions);
    setHistory(histoPositions);
    return () => {};
  }, [histoPositions]);
  */

  const reset = () => {
    setSelectedMarble(null);
    setTargetPosition(null);
  };

  const marbleClickHandler = (position) => {
    if (finished) {
      return;
    }
    if (Math.max(...scores[displayedMove]) === 6) {
      endgameHandler();
      return;
    }
    if (displayedMove < maxMove) {
      return;
    }
    if (selectedMarble === null) {
      if (!!(position % 2) !== turn) {
        return;
      }
    }

    if (selectedMarble === position) {
      reset();
    } else if (!!(position % 2) !== turn) {
      // may be a push or an impossible move
      setTargetPosition(marbleCoordinateToSquareCoordinate(position));
    } else {
      setSelectedMarble(position);
    }
  };

  // React.memo used in square imply we don't need to "re-read" any value. We must always do the same if we want to be able to memoize.
  // That's why the useEffect is useful here : instead of calling handleMove() here, we just set target position.
  // Then, the effect, which is not memoized, can read selectedMarble (which would be always equal to initialState if we tried to read from squareClick()), and compute the move.
  const squareClickHandler = (position) => {
    setTargetPosition(position);
  };

  const handleMove = () => {
    // this check in marbleClickHandler was not enough because player could have selected a marble from last move, then move backward in history to play from another position
    if (displayedMove < maxMove) {
      return;
    }
    if (selectedMarble === null) {
      return;
    }

    if (
      history[displayedMove].includes(
        squareCoordinateToMarbleCoordinate(targetPosition, (turn + 1) % 2),
      )
    ) {  // it was a push
      const computedMove = computeMove(
        marbleCoordinateToSquareCoordinate(selectedMarble),
        targetPosition,
        history[displayedMove],
        turn,
        true,
      );
      if (computedMove === null) {
        reset();
        return;
      }
      proceedMove(computedMove, true);
      // moveCallBack((previous) => previous + 1);
      // socketContext.socket.emit('game_move_play', {gameId: identifier, computedMove, pushed: false/*newPosition: history[maxMove]*/});
    } else { // not a push
      const computedMove = computeMove(
        marbleCoordinateToSquareCoordinate(selectedMarble),
        targetPosition,
        history[displayedMove],
        turn,
      );
      if (computedMove === null) {
        reset();
        return;
      }
      // use 3DCoordinates
      proceedMove(computedMove, false);
      // socketContext.socket.emit('game_move_play', {gameId: identifier, computedMove, pushed: false/*newPosition: history[maxMove]*/});
    }
  };

  const proceedMove = (computedMove, pushed) => {
    if(pushed) {
      // use 3DCoordinates
      const marbles = history[displayedMove].filter(
        (element) => !computedMove.move.oldCoordinates1D.includes(element),
      );
      computedMove.move.newCoordinates1D.forEach((element) => {
        marbles.push(
          // @TODO: push any marble in computedMove.move.newCoordinates1D
          element,
        );
      });
      if (computedMove.pushed) {
        setScores((previous) => {
          const blackScore = previous[previous.length - 1][0] + ((turn + 1) % 2);
          const whiteScore = previous[previous.length - 1][1] + (turn % 2);
          return [...previous, [blackScore, whiteScore]];
        });
      } else {
        setScores((previous) => [...previous, previous[previous.length - 1]]);
      }

      setVectors((previous) => [...previous, computedMove.vector]);
      setHistory((previous) => [...previous, marbles]);
      setSelectedMarble(null);
      setTargetPosition(null);
      toggleTurn((previous) => !previous);
      setMaxMove((previous) => previous + 1);
    } else {
      const marbles = history[displayedMove].filter(
        (element) => !computedMove.move.oldCoordinates1D.includes(element),
      );
      computedMove.move.newCoordinates1D.forEach((element) => {
        marbles.push(
          element,
        );
      });

      setVectors((previous) => [...previous, computedMove.vector]);
      setHistory((previous) => [...previous, marbles]);
      setScores((previous) => [...previous, previous[previous.length - 1]]);
      setSelectedMarble(null);
      setTargetPosition(null);
      toggleTurn((previous) => !previous);
      setMaxMove((previous) => previous + 1);
    }
  }

  const timeP1 = startClockP1.split(".");
  const clockP1 = (parseInt(timeP1[0])*60+parseInt(timeP1[1] ?? 0));
  const timerP1 = <Timer x="-10" y="210" value={clockP1} turn={!turn} started={started} />;

  const timeP2 = startClockP2.split(".");
  const clockP2 = (parseInt(timeP2[0])*60+parseInt(timeP2[1] ?? 0));
  const timerP2 = <Timer x="-10" y="90" value={clockP2} turn={turn} started={started} />;

  return (
    <>
      <Hexagon />
      <SquareList clickHandler={squareClickHandler} />
      <TriangleList />
      <Board
        selectedMarble={selectedMarble}
        marblesPositions={history[displayedMove]}
        marbleClickHandler={marbleClickHandler}
        marblesVector={vectors[displayedMove]}
        score={scores[displayedMove]}
        turn={turn}
      />
      {timerP1}
      {timerP2}
    </>
  );
};
