import {
  coordinates2D,
  coordinates3D,
  coordinates3DTo1D,
  squaresCoordinates,
} from '../Square/square.utils';
import {
  TOP_RIGHT,
  RIGHT,
  BOTTOM_RIGHT,
  TOP_LEFT,
  LEFT,
  BOTTOM_LEFT,
} from '../Arrow/arrow.utils';

/* axes values are incrementing in this order :

to move toward the top, increment the x axis
to move toward the right, increment the y & z axes

                   /\
                  /  \
                1z    9y
                /      \
     _________ / _ _ _ _\____9x___
    \         / o o - q q\        /
     \       / o o o q q q\      /
       \    / - o o - q q -\    /
        \  / - - - - - - - -\  /
          / - - - - - - - - -\/
         /\  - - - - - - - - /\
        /  \  - q q - o o - /  \
       /    \  q q q o o o /    \
      /      \  q q - o o /      \
     /___1x__ \_ _ _ _ _ /________\
               \        /
                \      /
                1y    9z
                  \  /
                   \/

*/

export const squareCoordinateToMarbleCoordinate = (coordinates1D, color) =>
  2 * coordinates1D + (color % 2);

export const marbleCoordinateToSquareCoordinate = (coordinates1D) =>
  Math.floor(coordinates1D / 2);

export const opponentTurn = (turn) => (turn + 1) % 2;

const getCoordinates1DApplyingVectorOnCoordinates3D = (
  coordinate3D,
  vector,
) => {
  if (vector === TOP_RIGHT) {
    return coordinates3DTo1D.indexOf(
      `${coordinates3D[coordinate3D][0] + 1}${
        coordinates3D[coordinate3D][1] + 1
      }${coordinates3D[coordinate3D][2]}`,
    );
  }
  if (vector === RIGHT) {
    return coordinates3DTo1D.indexOf(
      `${coordinates3D[coordinate3D][0]}${coordinates3D[coordinate3D][1] + 1}${
        coordinates3D[coordinate3D][2] + 1
      }`,
    );
  }
  if (vector === BOTTOM_RIGHT) {
    return coordinates3DTo1D.indexOf(
      `${coordinates3D[coordinate3D][0] - 1}${coordinates3D[coordinate3D][1]}${
        coordinates3D[coordinate3D][2] + 1
      }`,
    );
  }
  if (vector === TOP_LEFT) {
    return coordinates3DTo1D.indexOf(
      `${coordinates3D[coordinate3D][0] + 1}${coordinates3D[coordinate3D][1]}${
        coordinates3D[coordinate3D][2] - 1
      }`,
    );
  }
  if (vector === LEFT) {
    return coordinates3DTo1D.indexOf(
      `${
        `${coordinates3D[coordinate3D][0]}${coordinates3D[coordinate3D][1]}` - 1
      }${coordinates3D[coordinate3D][2] - 1}`,
    );
  }
  if (vector === BOTTOM_LEFT) {
    return coordinates3DTo1D.indexOf(
      `${
        `${coordinates3D[coordinate3D][0] - 1}${
          coordinates3D[coordinate3D][1]
        }` - 1
      }${coordinates3D[coordinate3D][2]}`,
    );
  }
};

const vector3D4LineMoves = {
  '1,0,0': [LEFT, RIGHT],
  '0,1,0': [BOTTOM_RIGHT, TOP_LEFT],
  '0,0,1': [BOTTOM_LEFT, TOP_RIGHT],
};

const vector3D4SideMovesOf2 = {
  '1,2,1': [RIGHT, TOP_RIGHT],
  '-1,1,2': [RIGHT, BOTTOM_RIGHT],
  '-1,-2,-1': [LEFT, BOTTOM_LEFT],
  '1,-1,-2': [LEFT, TOP_LEFT],
  '2,1,-1': [TOP_RIGHT, TOP_LEFT],
  '-2,-1,1': [BOTTOM_RIGHT, BOTTOM_LEFT],
};

const vector3D4SideMovesOf3 = {
  // vector to target squares, vector to next friendly marble to move
  '1,3,2': [TOP_RIGHT, RIGHT],
  '-1,-3,-2': [BOTTOM_LEFT, LEFT],
  '3,1,-2': [TOP_RIGHT, TOP_LEFT],
  '-3,-2,1': [BOTTOM_RIGHT, BOTTOM_LEFT],
  '-1,2,3': [BOTTOM_RIGHT, RIGHT],
  '1,-2,-3': [TOP_LEFT, LEFT],
  '3,2,-1': [TOP_LEFT, TOP_RIGHT],
  '-3,-1,2': [BOTTOM_LEFT, BOTTOM_RIGHT],
  '2,-1,-3': [LEFT, TOP_LEFT], // @todo check values are OK.
  '-2,1,3': [RIGHT, BOTTOM_RIGHT],
  '-2,-3,-1': [LEFT, BOTTOM_LEFT],
  '2,3,1': [RIGHT, TOP_RIGHT], // OK
};

export const computeMove = (from, to, marbles, turn, push = false) => {
  let pushed = false;
  const oldCoordinates1D = [];
  const newCoordinates1D = [];
  const arrowCoordinates1D = [];
  const diff = [
    coordinates3D[to][0] - coordinates3D[from][0],
    coordinates3D[to][1] - coordinates3D[from][1],
    coordinates3D[to][2] - coordinates3D[from][2],
  ];
  const distance = Math.max(...diff.map((e) => Math.abs(e)));
  const vector3D = [diff[0] / distance, diff[1] / distance, diff[2] / distance]; // @TODO: care because this can lead to float values. For side moves I decided to use diff constant instead

  if (distance > 3) {
    // remember this is distance between two clicks
    // -> even for pushes you will click at a max distance of 3 since you have to click on first marble to be pushed.
    return null;
  }

  if (push) {
    // @TODO: refacto using a new vector3D4Push.
    // from Game, we know it was a push because we clicked on opponent marble instead of a square.

    oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(from, turn));
    newCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
    arrowCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));

    let direction;
    if (diff[0] === 0) {
      direction = from > to ? 5 : 2;
    }
    if (diff[1] === 0) {
      direction = from > to ? 3 : 6;
    }
    if (diff[2] === 0) {
      direction = from > to ? 4 : 1;
    }

    // we need to check this marble can push by checking there are marbles of the same color on the path
    // so we apply vector from "from" coordinates to check if it is in the list of marbles
    const nextSquareCoordinates3DString = `${
      coordinates3D[from][0] + vector3D[0]
    }${coordinates3D[from][1] + vector3D[1]}${
      coordinates3D[from][2] + vector3D[2]
    }`;
    const nextSquareCoordinates1D = coordinates3DTo1D.indexOf(
      nextSquareCoordinates3DString,
    );

    if (
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D, turn),
      )
    ) {
      // can't push if there is not our marble on next square
      return null;
    }

    const nextSquareCoordinates3DString2 = `${
      parseInt(nextSquareCoordinates3DString[0]) + vector3D[0]
    }${parseInt(nextSquareCoordinates3DString[1]) + vector3D[1]}${
      parseInt(nextSquareCoordinates3DString[2]) + vector3D[2]
    }`;
    const nextSquareCoordinates1D2 = coordinates3DTo1D.indexOf(
      nextSquareCoordinates3DString2,
    );

    if (distance === 2) {
      // if was a 2v1 push
      if (
        !marbles.includes(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D2,
            opponentTurn(turn),
          ),
        )
      ) {
        // I think this one is not necessary since we are supposed to have clicked on it.
        return null;
      }

      // check the square about to receive the marble pushed is empty
      const nextSquareCoordinates3DString3 = `${
        parseInt(nextSquareCoordinates3DString2[0]) + vector3D[0]
      }${parseInt(nextSquareCoordinates3DString2[1]) + vector3D[1]}${
        parseInt(nextSquareCoordinates3DString2[2]) + vector3D[2]
      }`;
      const nextSquareCoordinates1D3 = coordinates3DTo1D.indexOf(
        nextSquareCoordinates3DString3,
      );
      if (
        marbles.includes(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D3,
            opponentTurn(turn),
          ),
        ) ||
        marbles.includes(
          squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D3, turn),
        )
      ) {
        return null;
      }

      oldCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D2,
          opponentTurn(turn),
        ),
      );

      if (!coordinates2D[nextSquareCoordinates1D3]) {
        // pushed out
        pushed = true;
      } else {
        newCoordinates1D.push(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D3,
            opponentTurn(turn),
          ),
        );

        arrowCoordinates1D.push(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D3,
            opponentTurn(turn),
          ),
        );
      }

      return {
        vector: {
          direction,
          arrowPositions: arrowCoordinates1D,
        },
        move: { oldCoordinates1D, newCoordinates1D },
        pushed,
      };
    }

    // we are in a 3v1 or 3v2 push, so we need a third marble there
    if (
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D2, turn),
      )
    ) {
      return null;
    }
    // make sure the next square contains opponent marble
    const nextSquareCoordinates3DString3 = `${
      parseInt(nextSquareCoordinates3DString2[0]) + vector3D[0]
    }${parseInt(nextSquareCoordinates3DString2[1]) + vector3D[1]}${
      parseInt(nextSquareCoordinates3DString2[2]) + vector3D[2]
    }`;
    const nextSquareCoordinates1D3 = coordinates3DTo1D.indexOf(
      nextSquareCoordinates3DString3,
    );
    if (
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D3,
          opponentTurn(turn),
        ),
      )
    ) {
      return null;
    }

    // 3v1: if next square is empty, we can push
    const nextSquareCoordinates3DString4 = `${
      parseInt(nextSquareCoordinates3DString3[0]) + vector3D[0]
    }${parseInt(nextSquareCoordinates3DString3[1]) + vector3D[1]}${
      parseInt(nextSquareCoordinates3DString3[2]) + vector3D[2]
    }`;
    const nextSquareCoordinates1D4 = coordinates3DTo1D.indexOf(
      nextSquareCoordinates3DString4,
    );
    if (
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D4,
          opponentTurn(turn),
        ),
      ) &&
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D4, turn),
      )
    ) {
      oldCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D3,
          opponentTurn(turn),
        ),
      );

      if (!coordinates2D[nextSquareCoordinates1D4]) {
        // pushed out
        pushed = true;
      } else {
        newCoordinates1D.push(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D4,
            opponentTurn(turn),
          ),
        );
        arrowCoordinates1D.push(
          squareCoordinateToMarbleCoordinate(
            nextSquareCoordinates1D4,
            opponentTurn(turn),
          ),
        );
      }
      return {
        vector: {
          direction,
          arrowPositions: arrowCoordinates1D,
        },
        move: { oldCoordinates1D, newCoordinates1D },
        pushed,
      };
    }
    // 3v2: else we have to check if the square is an opponent marble AND next square is empty (or outside of the board)
    if (
      !marbles.includes(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D4,
          opponentTurn(turn),
        ),
      )
    ) {
      return null;
    }

    const nextSquareCoordinates3DString5 = `${
      parseInt(nextSquareCoordinates3DString4[0]) + vector3D[0]
    }${parseInt(nextSquareCoordinates3DString4[1]) + vector3D[1]}${
      parseInt(nextSquareCoordinates3DString4[2]) + vector3D[2]
    }`;
    const nextSquareCoordinates1D5 = coordinates3DTo1D.indexOf(
      nextSquareCoordinates3DString5,
    );
    if (
      marbles.includes(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D5,
          opponentTurn(turn),
        ),
      ) ||
      marbles.includes(
        squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D5, turn),
      )
    ) {
      return null;
    }
    oldCoordinates1D.push(
      squareCoordinateToMarbleCoordinate(
        nextSquareCoordinates1D3,
        opponentTurn(turn),
      ),
    );
    if (!coordinates2D[nextSquareCoordinates1D5]) {
      // pushed out
      pushed = true;
    } else {
      newCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D5,
          opponentTurn(turn),
        ),
      );

      arrowCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(
          nextSquareCoordinates1D5,
          opponentTurn(turn),
        ),
      );
    }
    return {
      vector: {
        direction,
        arrowPositions: arrowCoordinates1D,
      },
      move: { oldCoordinates1D, newCoordinates1D },
      pushed,
    };
  }

  // SIDE MOVE : x,y,z are changing
  if (
    coordinates3D[from][0] !== coordinates3D[to][0] &&
    coordinates3D[from][1] !== coordinates3D[to][1] &&
    coordinates3D[from][2] !== coordinates3D[to][2]
  ) {
    const diffString = diff.toString();

    // distance is representing the number of marbles we will move
    if (distance == 2) {
      // In Abalone a vector describing a side move of 2 marbles can be shared between 2 different side moves.
      // Fortunately (or not), they are mutually exclusive, meaning that if one is possible, it was this one that was asked.
      // Else, it may be the other one.

      const aSquare = getCoordinates1DApplyingVectorOnCoordinates3D(
        from,
        vector3D4SideMovesOf2[diffString][0],
      );
      const bSquare = getCoordinates1DApplyingVectorOnCoordinates3D(
        from,
        vector3D4SideMovesOf2[diffString][1],
      );
      if (marbles.includes(squareCoordinateToMarbleCoordinate(aSquare, turn))) {
        if (
          !marbles.includes(
            squareCoordinateToMarbleCoordinate(bSquare, turn),
          ) &&
          !marbles.includes(
            squareCoordinateToMarbleCoordinate(bSquare, opponentTurn(turn)),
          )
        ) {
          oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(from, turn));
          newCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
          arrowCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
          oldCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(aSquare, turn),
          );
          newCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(bSquare, turn),
          );
          arrowCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(bSquare, turn),
          );
          return {
            vector: {
              direction: vector3D4SideMovesOf2[diffString][1],
              arrowPositions: arrowCoordinates1D,
            },
            move: { oldCoordinates1D, newCoordinates1D },
          };
        }
      } else if (
        marbles.includes(squareCoordinateToMarbleCoordinate(bSquare, turn))
      ) {
        if (
          !marbles.includes(
            squareCoordinateToMarbleCoordinate(aSquare, opponentTurn(turn)),
          )
        ) {
          oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(from, turn));
          newCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
          arrowCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
          oldCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(bSquare, turn),
          );
          newCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(aSquare, turn),
          );
          arrowCoordinates1D.push(
            squareCoordinateToMarbleCoordinate(aSquare, turn),
          );
          return {
            vector: {
              direction: vector3D4SideMovesOf2[diffString][0],
              arrowPositions: arrowCoordinates1D,
            },
            move: { oldCoordinates1D, newCoordinates1D },
          };
        }
        return null;
      } else {
        return null;
      }
    } else {
      // 3 marbles moving in side move

      if (!vector3D4SideMovesOf3[diffString]) {
        // some weird move
        return null;
      }

      // get 2 other marbles ready to move squares coordinates
      const aSquare = getCoordinates1DApplyingVectorOnCoordinates3D(
        from,
        vector3D4SideMovesOf3[diffString][1],
      );
      const a2Square = getCoordinates1DApplyingVectorOnCoordinates3D(
        aSquare,
        vector3D4SideMovesOf3[diffString][1],
      );

      // get 2 target squares coordinates
      const bSquare = getCoordinates1DApplyingVectorOnCoordinates3D(
        from,
        vector3D4SideMovesOf3[diffString][0],
      );
      const b2Square = getCoordinates1DApplyingVectorOnCoordinates3D(
        aSquare,
        vector3D4SideMovesOf3[diffString][0],
      );

      // check we have the 2 marbles required to make a move of distance 3
      if (
        !marbles.includes(squareCoordinateToMarbleCoordinate(aSquare, turn)) ||
        !marbles.includes(squareCoordinateToMarbleCoordinate(a2Square, turn))
      ) {
        return null;
      }
      // check target squares are empty
      if (
        marbles.includes(squareCoordinateToMarbleCoordinate(bSquare, turn)) ||
        marbles.includes(
          squareCoordinateToMarbleCoordinate(bSquare, opponentTurn(turn)),
        ) ||
        marbles.includes(squareCoordinateToMarbleCoordinate(b2Square, turn)) ||
        marbles.includes(
          squareCoordinateToMarbleCoordinate(b2Square, opponentTurn(turn)),
        )
      ) {
        return null;
      }

      oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(from, turn));
      newCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
      arrowCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
      oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(aSquare, turn));
      newCoordinates1D.push(squareCoordinateToMarbleCoordinate(bSquare, turn));
      arrowCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(bSquare, turn),
      );
      oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(a2Square, turn));
      newCoordinates1D.push(squareCoordinateToMarbleCoordinate(b2Square, turn));
      arrowCoordinates1D.push(
        squareCoordinateToMarbleCoordinate(b2Square, turn),
      );

      return {
        vector: {
          direction: vector3D4SideMovesOf3[diffString][0],
          arrowPositions: arrowCoordinates1D,
        },
        move: { oldCoordinates1D, newCoordinates1D },
      };
    }
  }

  // LINE MOVE : only 1 axis has the same value.
  // if max diff = 3 : 1 marble jumped over 2 others (3 marbles move)
  // if max diff = 2 : 1 marble jumped over 1 (2 marbles move)
  // if max diff = 1 : moving only 1
  oldCoordinates1D.push(squareCoordinateToMarbleCoordinate(from, turn));
  newCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
  if (
    (diff[0] === 0 && diff[1] !== 0 && diff[2] !== 0) ||
    (diff[0] !== 0 && diff[1] === 0 && diff[2] !== 0) ||
    (diff[0] !== 0 && diff[1] !== 0 && diff[2] === 0)
  ) {
    if (distance > 1) {
      // we need to check this marble can travel so far by checking there are marbles of the same color on the path
      // so we apply vector from "from" coordinates to check if it is in the list of marbles
      const nextSquareCoordinates3DString = `${
        coordinates3D[from][0] + vector3D[0]
      }${coordinates3D[from][1] + vector3D[1]}${
        coordinates3D[from][2] + vector3D[2]
      }`;
      const nextSquareCoordinates1D = coordinates3DTo1D.indexOf(
        nextSquareCoordinates3DString,
      );
      if (
        !marbles.includes(
          squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D, turn),
        )
      ) {
        return null;
      }
      if (distance > 2) {
        // reapply vector to check.
        const nextSquareCoordinates3DString2 = `${
          parseInt(nextSquareCoordinates3DString[0]) + vector3D[0]
        }${parseInt(nextSquareCoordinates3DString[1]) + vector3D[1]}${
          parseInt(nextSquareCoordinates3DString[2]) + vector3D[2]
        }`;
        const nextSquareCoordinates1D2 = coordinates3DTo1D.indexOf(
          nextSquareCoordinates3DString2,
        );
        if (
          !marbles.includes(
            squareCoordinateToMarbleCoordinate(nextSquareCoordinates1D2, turn),
          )
        ) {
          return null;
        }
      }
    }
    arrowCoordinates1D.push(squareCoordinateToMarbleCoordinate(to, turn));
    return {
      vector: {
        direction:
          vector3D4LineMoves[
            `${!diff[0] ? 1 : 0},${!diff[1] ? 1 : 0},${!diff[2] ? 1 : 0}`
          ][from > to ? 0 : 1],
          arrowPositions: arrowCoordinates1D,
      },
      move: { oldCoordinates1D, newCoordinates1D },
    };
  }

  return null;
};
export const variantToMap = {
  belgian_daisy: [
    0,
    2,
    10,
    12,
    14,
    24,
    26,
    94,
    96,
    106,
    108,
    110,
    118,
    120, // up to this point, black marbles, since those are even numbers
    7,
    9,
    17,
    19,
    21,
    31,
    33,
    89,
    91,
    101,
    103,
    105,
    113,
    115,
  ],
  the_clearing: [
    2,
    6,
    10,
    20,
    26,
    36,
    48,
    68,
    70,
    88,
    98,
    112,
    116,
    120,
    1,
    5,
    9,
    23,
    33,
    51,
    53,
    73,
    85,
    95,
    101,
    111,
    115,
    119,
  ],
  german_daisy: [
    10,
    12,
    22,
    24,
    26,
    38,
    40,
    80,
    82,
    94,
    96,
    98,
    108,
    110,
    19,
    21,
    31,
    33,
    35,
    47,
    49,
    73,
    75,
    87,
    89,
    91,
    101,
    103,
  ],
  star: [
    0,
    18,
    26,
    34,
    44,
    54,
    58,
    64,
    68,
    78,
    90,
    98,
    108,
    112,
    9,
    13,
    23,
    31,
    43,
    53,
    57,
    63,
    67,
    77,
    87,
    95,
    103,
    121
  ],
  dutch_daisy: [
    0,
    2,
    10,
    14,
    18,
    24,
    26,
    94,
    96,
    102,
    106,
    110,
    118,
    120, // up to this point, black marbles, since those are even numbers
    7,
    9,
    13,
    17,
    21,
    31,
    33,
    89,
    91,
    101,
    105,
    109,
    113,
    115,
  ],
  swiss_daisy: [
    10,
    12,
    22,
    26,
    32,
    38,
    40,
    80,
    82,
    88,
    94,
    98,
    108,
    110,
    19,
    21,
    25,
    31,
    35,
    47,
    49,
    73,
    75,
    87,
    91,
    97,
    101,
    103,
  ],
  snakes_variant: [
    38,
    40,
    42,
    44,
    46,
    54,
    70,
    86,
    100,
    112,
    114,
    116,
    118,
    120,
    1,
    3,
    5,
    7,
    9,
    21,
    35,
    51,
    67,
    75,
    77,
    79,
    81,
    83,
  ],
  alien: [
    14,
    16,
    26,
    30,
    76,
    78,
    88,
    92,
    96,
    102,
    108,
    112,
    116,
    120,
    1,
    5,
    9,
    13,
    19,
    25,
    29,
    33,
    43,
    45,
    91,
    95,
    105,
    107
  ],
  the_wall: [
    4,
    24,
    26,
    28,
    30,
    32,
    36,
    38,
    40,
    42,
    44,
    46,
    48,
    50,
    71,
    73,
    75,
    77,
    79,
    81,
    83,
    85,
    89,
    91,
    93,
    95,
    97,
    117
  ],
  duel: [
    6,
    16,
    18,
    20,
    30,
    36,
    52,
    54,
    70,
    94,
    106,
    108,
    110,
    118,
    3,
    11,
    13,
    15,
    27,
    51,
    67,
    69,
    85,
    91,
    101,
    103,
    105,
    115
  ],
  centrifugeuse: [
    2,
    4,
    6,
    28,
    70,
    72,
    82,
    84,
    86,
    88,
    96,
    98,
    100,
    110,
    11,
    21,
    23,
    25,
    33,
    35,
    37,
    39,
    49,
    51,
    93,
    115,
    117,
    119
  ],
  atomouche: [
    6,
    12,
    20,
    36,
    46,
    58,
    68,
    70,
    90,
    106,
    110,
    112,
    9,
    11,
    15,
    31,
    51,
    53,
    63,
    75,
    85,
    101,
    109,
    115
  ],
  corners: [
    0,
    2,
    10,
    18,
    50,
    54,
    68,
    76,
    84,
    100,
    108,
    112,
    114,
    7,
    9,
    13,
    21,
    37,
    45,
    53,
    67,
    71,
    103,
    111,
    119,
    121
  ],
  alliances: [
    16,
    18,
    28,
    32,
    42,
    48,
    58,
    66,
    76,
    82,
    94,
    96,
    106,
    108,
    13,
    15,
    25,
    27,
    39,
    45,
    55,
    63,
    73,
    79,
    89,
    93,
    103,
    105
  ],
  domination: [
    10,
    22,
    24,
    36,
    38,
    40,
    58,
    62,
    80,
    82,
    84,
    96,
    98,
    110,
    21,
    33,
    35,
    45,
    47,
    49,
    51,
    71,
    73,
    75,
    77,
    87,
    89,
    101
  ],
  fujiyama: [
    0,
    2,
    4,
    6,
    8,
    12,
    18,
    26,
    30,
    42,
    44,
    92,
    104,
    106,
    15,
    17,
    29,
    77,
    79,
    91,
    95,
    103,
    109,
    113,
    115,
    117,
    119,
    121
  ],
  standard: [
    0,
    2,
    4,
    6,
    8,
    10,
    12,
    14,
    16,
    18,
    20,
    26,
    28,
    30,
    91,
    93,
    95,
    101,
    103,
    105,
    107,
    109,
    111,
    113,
    115,
    117,
    119,
    121
  ],
  accelium: [
    0,
    2,
    4,
    6,
    8,
    12,
    14,
    16,
    18,
    26,
    28,
    30,
    91,
    93,
    95,
    103,
    105,
    107,
    109,
    113,
    115,
    117,
    119,
    121
  ],
};
/* need to implement at least those :
          <option value="snakes_variant">Snakes Variant</option>
          <option value="alien">Alien Attack</option>
          <option value="the_wall">The Wall</option>
          <option value="duel">Duel</option>
          <option value="centrifugeuse">Centrifugeuse</option>
          <option value="atomouche">Atomouche</option>
          <option value="corner">Corner</option>
          <option value="alliances">Alliances</option>
          <option value="domination">Domination</option>
          <option value="fujiyama">Fujiyama</option>
          <option value="standard">Standard</option>
          <option value="accelium">Clash (Accelium)</option>

And add a way to describe his own variant with an abaCode
*/