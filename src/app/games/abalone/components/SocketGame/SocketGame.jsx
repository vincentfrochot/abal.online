import React, { useState, useContext, useEffect } from 'react';
import { Hexagon } from '../Hexagon/Hexagon';
import { SquareList } from '../SquareList/SquareList';
import { TriangleList } from '../TriangleList/TriangleList';
import { Board } from '../Board/Board';
import GamesContext from '../../../../contexts/GamesContext';
import UserContext from '../../../../contexts/UserContext';
import { Timer } from '../../../core/components/Timer/Timer';
import {
  squareCoordinateToMarbleCoordinate,
  marbleCoordinateToSquareCoordinate,
  computeMove,
} from '../Game/game.utils';
import {
  SocketContext,
  SOCKET_EVENTS,
} from '../../../../contexts/SocketContext';
import { checkPropTypes } from 'prop-types';

export const SocketGame = (props) => {
  const {
    displayedMoveIndex, // chosen from the gameCard
    moveCallBack, // increment the gameCard displayedMoveIndex
    endgameHandler,
    sidentifier,
  } = props;
  const userContext = useContext(UserContext);
  const { gamesContext } = useContext(GamesContext);
  const socketContext = useContext(SocketContext);
  // socket game
  const sgame = gamesContext.games.find((e) => e.id === sidentifier);
  if (!sgame) {
    endgameHandler(sidentifier);
  }
  const samIBlack =
    sgame.players[0] && sgame.players[0] === userContext.userContext.nickname;
  const samIWhite =
    sgame.players[1] && sgame.players[1] === userContext.userContext.nickname;
  const sblackTurn = sgame.lastMoveIndex % 2 === 0;
  const samIPlaying = samIBlack || samIWhite;
  const stimeP1 = sgame.startClockP1.split('.');
  const sclockP1 = parseInt(stimeP1[0]) * 60 + parseInt(stimeP1[1] ?? 0);
  const stimeP2 = sgame.startClockP2.split('.');
  const sclockP2 = parseInt(stimeP2[0]) * 60 + parseInt(stimeP2[1] ?? 0);
  const stype = sgame.type;
  const refTime = sgame.histoTimes[sgame.lastMoveIndex];

  // local game
  const [selectedMarble, setSelectedMarble] = useState(null); // 1st click, marble actually selected (and by extension, the square we leave)
  const [targetSquare, setTargetSquare] = useState(null); // second click square coordinates (the square we want to reach)
  const [turn, toggleTurn] = useState(sgame.lastMoveIndex % 2 === 1); // is it Black or White turn
  const [scores, setScores] = useState(sgame.histoScores);
  const [history, setHistory] = useState(sgame.histoPositions);
  const [vectors, setVectors] = useState(sgame.histoVectors);
  const [times, setTimes] = useState([Date.now()]);
  const [clocks, setClocks] = useState([0, 0]);
  const [lastMoveIndex, setLastMoveIndex] = useState(0);

  const shouldIPlay =
    ((sblackTurn && samIBlack && !turn) ||
      (!sblackTurn && samIWhite && turn)) &&
    sgame.started;


  // @TODO: determine HERE if we send moves, depending on client color. Determine client color using GamesContext.games.players[] to compare with our nickname.

  // const iAmBlack =
  //   game.players[0] !== null &&
  //   game.players[0].nickname === userContext.nickname;
  // const iAmWhite =
  //   game.players[1] !== null &&
  //   game.players[1].nickname === userContext.nickname;

  // const [iHaveToPlay, setIHaveToPlay] = useState(iAmBlack); // only first player will "send" first move

  // const [lastMoveIndex, setLastMoveIndex] = useState(0);
  // const [smyturn, setSMyTurn] = useState(iAmBlack);

  const displayedMove =
    displayedMoveIndex > sgame.lastMoveIndex
      ? sgame.lastMoveIndex
      : displayedMoveIndex;

  // forced to use useEffect() instead of triggering the state change inside to avoid re-generating a new function each time for squareClickHandler.
  useEffect(() => {
    // remember useEffect is to tell React to call this function AFTER targetSquare was changed and the DOM rendered.
    handleMove();
  }, [targetSquare]);

  const reset = () => {
    setSelectedMarble(null);
    setTargetSquare(null);
  };

  const marbleClickHandler = (position) => {
    if (sgame.finished) {
      // @TODO: check how to adapt.
      endgameHandler();
      return;
    }

    if (sgame.type === 'match') {
      if (!shouldIPlay) {
        return;
      }
    }

    if (Math.max(...scores[lastMoveIndex]) === 6) {
      endgameHandler();
      return;
    }
    // if (displayedMove < lastMoveInde) {
    //   return;
    // }
    if (selectedMarble === null) {
      if (!!(position % 2) !== turn) {
        return;
      }
    }

    if (selectedMarble === position) {
      reset();
    } else if (!!(position % 2) !== turn) {
      // may be a push or an impossible move
      setTargetSquare(marbleCoordinateToSquareCoordinate(position));
    } else {
      setSelectedMarble(position);
    }
  };

  const squareClickHandler = (position) => {
    setTargetSquare(position);
  };

  const handleMove = () => {
    // this check in marbleClickHandler was not enough because player could have selected a marble from last move, then move backward in history to play from another position
    // if (displayedMove < lastMoveIndex) {
    //   return;
    // }
    if (selectedMarble === null) {
      return;
    }

    if (
      sgame.histoPositions[sgame.lastMoveIndex].includes(
        squareCoordinateToMarbleCoordinate(targetSquare, (turn + 1) % 2),
      )
    ) {
      // it was a push
      const computedMove = computeMove(
        marbleCoordinateToSquareCoordinate(selectedMarble),
        targetSquare,
        sgame.histoPositions[sgame.lastMoveIndex],
        turn,
        true,
      );
      if (computedMove === null) {
        reset();
        return;
      }
      const pushedOut = proceedMove(computedMove, true);
      // moveCallBack((previous) => previous + 1);
      if (sgame.started && sgame.type === 'match') {
        socketContext.socket.emit('game_move_play', {
          gameId: sidentifier,
          computedMove,
          pushed: pushedOut,
        });
      }
      if (sgame.type === 'analysis') {
        socketContext.socket.emit('analysis_move_play', {
          gameId: sidentifier,
          computedMove,
          pushed: pushedOut,
          index: displayedMoveIndex
        });
      }
    } else {
      // not a push
      const computedMove = computeMove(
        marbleCoordinateToSquareCoordinate(selectedMarble),
        targetSquare,
        sgame.histoPositions[sgame.lastMoveIndex],
        turn,
      );
      if (computedMove === null) {
        reset();
        return;
      }
      // use 3DCoordinates
      proceedMove(computedMove, false);

      if (sgame.started && sgame.type === 'match') {
        socketContext.socket.emit('game_move_play', {
          gameId: sidentifier,
          computedMove,
          pushed: false,
        });
      }
      if (sgame.type === 'analysis') {
        socketContext.socket.emit('analysis_move_play', {
          gameId: sidentifier,
          computedMove,
          pushed: false,
          index: displayedMoveIndex
        });
      }
    }
  };

  const proceedMove = (computedMove, pushed) => {
    if (pushed) {
      // use 3DCoordinates
      const marbles = sgame.histoPositions[sgame.lastMoveIndex].filter(
        (element) => !computedMove.move.oldCoordinates1D.includes(element),
      );
      computedMove.move.newCoordinates1D.forEach((element) => {
        marbles.push(
          // @TODO: push any marble in computedMove.move.newCoordinates1D
          element,
        );
      });
      if (computedMove.pushed) {
        setScores((previous) => {
          const blackScore =
            previous[previous.length - 1][0] + ((turn + 1) % 2);
          const whiteScore = previous[previous.length - 1][1] + (turn % 2);
          return [...previous, [blackScore, whiteScore]];
        });
      } else {
        setScores((previous) => [...previous, previous[previous.length - 1]]);
      }

      setVectors((previous) => [...previous, computedMove.vector]);
      setHistory((previous) => [...previous, marbles]);
      setTimes((previous) => [...previous, Date.now()]);
      setSelectedMarble(null);
      setTargetSquare(null);
      toggleTurn((previous) => !previous);
      setLastMoveIndex((previous) => previous + 1);
      if (computedMove.pushed) {
        return true;
      }
    } else {
      const marbles = sgame.histoPositions[sgame.lastMoveIndex].filter(
        (element) => !computedMove.move.oldCoordinates1D.includes(element),
      );
      computedMove.move.newCoordinates1D.forEach((element) => {
        marbles.push(element);
      });

      setVectors((previous) => [...previous, computedMove.vector]);
      setHistory((previous) => [...previous, marbles]);
      setScores((previous) => [...previous, previous[previous.length - 1]]);
      setTimes((previous) => [...previous, Date.now()]);
      setSelectedMarble(null);
      setTargetSquare(null);
      toggleTurn((previous) => !previous);
      setLastMoveIndex((previous) => previous + 1);
    }
  };

  const outOfTimeHandler = () => {
    if (!sgame.finished && samIPlaying) {
      socketContext.socket.emit(SOCKET_EVENTS.client.game.game_win_time, {
        gameId: sidentifier,
      });
    }
  }

  let marblesPositions, marblesVector, score;

  // if (lastMoveIndex > moveIndex) {
  //   marblesPositions = history[lastMoveIndex];
  //   marblesVector = vectors[lastMoveIndex];
  //   score = scores[lastMoveIndex];
  // } else {
  //   marblesPositions = sgame.histoPositions[lastMoveIndex];
  //   marblesVector = sgame.histoVectors[lastMoveIndex];
  //   score = sgame.histoScores[lastMoveIndex];
  // }

  // const marblesPositions = history[displayedMoveIndex];
  // const score = scores[displayedMoveIndex];
  // const marblesVector = vectors[displayedMoveIndex];

  marblesPositions = sgame.histoPositions[sgame.lastMoveIndex];
  score = sgame.histoScores[sgame.lastMoveIndex];
  marblesVector = sgame.histoVectors[sgame.lastMoveIndex];

  if (sgame.finished) {
    marblesPositions = sgame.histoPositions[displayedMove];
    score = sgame.histoScores[displayedMove];
    marblesVector = sgame.histoVectors[displayedMove];
  }

  if (sgame.type === "analysis") {
    marblesPositions = sgame.histoPositions[displayedMoveIndex];
    score = sgame.histoScores[displayedMoveIndex];
    marblesVector = sgame.histoVectors[displayedMoveIndex];
  }

  return (
    <>
      <Hexagon gametype={sgame.type} finished={sgame.finished} />
      <SquareList clickHandler={squareClickHandler} />
      <TriangleList />
      <Board
        selectedMarble={selectedMarble}
        marbleClickHandler={marbleClickHandler}
        marblesPositions={marblesPositions}
        marblesVector={marblesVector}
        score={score}
        turn={turn}
        myColor={samIBlack ? "black" :  samIWhite ? "white" : "none"}
      />
      {sgame.players[0] && (
        <text x="-40" y="220" style={{ font: 'bold 6.5px arial' }}>
          {sgame.players[0]}
        </text>
      )}
      <Timer
        x="-10"
        y="210"
        value={sgame.type === 'analysis' ? clocks[lastMoveIndex] : sclockP1}
        turn={sgame.type === 'analysis' ? !turn : sblackTurn}
        started={sgame.started}
        finished={sgame.finished}
        refTime={sgame.type === 'analysis' ? times[lastMoveIndex] : refTime}
        type={sgame.type}
        outOfTimeHandler={outOfTimeHandler}
      />

      {sgame.players[1] && (
        <text x="-40" y="70" style={{ font: 'bold 6.5px arial' }}>
          {sgame.players[1]}
        </text>
      )}

      <Timer
        x="-10"
        y="90"
        value={sgame.type === 'analysis' ? clocks[lastMoveIndex] : sclockP2}
        turn={sgame.type === 'analysis' ? turn : !sblackTurn}
        started={sgame.started}
        finished={sgame.finished}
        refTime={sgame.type === 'analysis' ? times[lastMoveIndex] : refTime}
        type={sgame.type}
        outOfTimeHandler={outOfTimeHandler}
      />
    </>
  );
};
