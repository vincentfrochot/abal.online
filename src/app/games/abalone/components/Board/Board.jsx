import React from 'react';
import { MarbleList } from '../MarbleList/MarbleList';
import { squaresCoordinates } from '../Square/square.utils';
import { SvgText } from '../../../core/components/SvgText/SvgText';

export const Board = (props) => {
  const {
    marblesPositions,
    marbleClickHandler,
    selectedMarble,
    marblesVector,
    score,
    turn,
    myColor,
    // readOnly,
    // snapshopMode,
    ...other
  } = props;

  const coordinatesColor = 'black';
  const fontSize = 8;
  const coordinates = [
    <SvgText
      color={coordinatesColor}
      key={'a' + coordinatesColor}
      label="a"
      fontSize={fontSize}
      x={squaresCoordinates[1][0].x + 7}
      y={squaresCoordinates[1][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'b' + coordinatesColor}
      label="b"
      fontSize={fontSize}
      x={squaresCoordinates[2][0].x + 7}
      y={squaresCoordinates[2][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'c' + coordinatesColor}
      label="c"
      fontSize={fontSize}
      x={squaresCoordinates[3][0].x + 7}
      y={squaresCoordinates[3][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'d' + coordinatesColor}
      label="d"
      fontSize={fontSize}
      x={squaresCoordinates[4][0].x + 7}
      y={squaresCoordinates[4][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'e' + coordinatesColor}
      label="e"
      fontSize={fontSize}
      x={squaresCoordinates[5][0].x + 7}
      y={squaresCoordinates[5][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'f' + coordinatesColor}
      label="f"
      fontSize={fontSize}
      x={squaresCoordinates[6][0].x + 7}
      y={squaresCoordinates[6][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'g' + coordinatesColor}
      label="g"
      fontSize={fontSize}
      x={squaresCoordinates[7][0].x + 7}
      y={squaresCoordinates[7][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'h' + coordinatesColor}
      label="h"
      fontSize={fontSize}
      x={squaresCoordinates[8][0].x + 7}
      y={squaresCoordinates[8][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'i' + coordinatesColor}
      label="i"
      fontSize={fontSize}
      x={squaresCoordinates[9][0].x + 7}
      y={squaresCoordinates[9][0].y + 3}
    />,
    <SvgText
      color={coordinatesColor}
      key={'1' + coordinatesColor}
      label="1"
      fontSize={fontSize}
      x={squaresCoordinates[0][1].x - 6}
      y={squaresCoordinates[0][1].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'2' + coordinatesColor}
      label="2"
      fontSize={fontSize}
      x={squaresCoordinates[0][2].x - 6}
      y={squaresCoordinates[0][2].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'3' + coordinatesColor}
      label="3"
      fontSize={fontSize}
      x={squaresCoordinates[0][3].x - 6}
      y={squaresCoordinates[0][3].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'4' + coordinatesColor}
      label="4"
      fontSize={fontSize}
      x={squaresCoordinates[0][4].x - 6}
      y={squaresCoordinates[0][4].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'5' + coordinatesColor}
      label="5"
      fontSize={fontSize}
      x={squaresCoordinates[0][5].x - 5}
      y={squaresCoordinates[0][5].y - 5}
    />,
    <SvgText
      color={coordinatesColor}
      key={'6' + coordinatesColor}
      label="6"
      fontSize={fontSize}
      x={squaresCoordinates[1][6].x - 6}
      y={squaresCoordinates[1][6].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'7' + coordinatesColor}
      label="7"
      fontSize={fontSize}
      x={squaresCoordinates[2][7].x - 6}
      y={squaresCoordinates[2][7].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'8' + coordinatesColor}
      label="8"
      fontSize={fontSize}
      x={squaresCoordinates[3][8].x - 6}
      y={squaresCoordinates[3][8].y - 4}
    />,
    <SvgText
      color={coordinatesColor}
      key={'9' + coordinatesColor}
      label="9"
      fontSize={fontSize}
      x={squaresCoordinates[4][9].x - 6}
      y={squaresCoordinates[4][9].y - 4}
    />,
  ];

  return (
    <>
      <MarbleList
        selected={selectedMarble}
        positions={marblesPositions}
        clickHandler={marbleClickHandler}
        vector={marblesVector}
        score={score}
        turn={turn}
        myColor={myColor}
      />
      {coordinates}
    </>
  );
};

Board.propTypes = {};
