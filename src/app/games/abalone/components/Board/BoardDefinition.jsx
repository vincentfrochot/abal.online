import React from 'react';

import { squareRadius } from '../Square/square.utils';

import './board.scss';

export const BoardDefinition = () => (
  <svg viewBox="0 0 100 100" width="0px" height="0px">
    <defs>
      <filter id="f3" height="130%">
        <feOffset
          xmlns="http://www.w3.org/2000/svg"
          dx="2"
          dy="2"
          result="offsetblur"
        />
        <feGaussianBlur
          xmlns="http://www.w3.org/2000/svg"
          in="SourceAlpha"
          stdDeviation="2"
        />
        <feComponentTransfer xmlns="http://www.w3.org/2000/svg">
          <feFuncA type="linear" slope="2" />
        </feComponentTransfer>
        <feMerge xmlns="http://www.w3.org/2000/svg">
          <feMergeNode />
          <feMergeNode in="SourceGraphic" />
        </feMerge>
      </filter>
      <filter id="triangleShadow" x="-50%" y="-50%" width="180%" height="180%">
        <feOffset
          xmlns="http://www.w3.org/2000/svg"
          dx="2"
          dy="2"
          result="offsetblur"
        />
        <feGaussianBlur
          xmlns="http://www.w3.org/2000/svg"
          in="SourceAlpha"
          stdDeviation="0.9"
        />
        <feComponentTransfer xmlns="http://www.w3.org/2000/svg">
          <feFuncA type="linear" slope="2.3" />
        </feComponentTransfer>
        <feMerge xmlns="http://www.w3.org/2000/svg">
          <feMergeNode />
          <feMergeNode in="SourceGraphic" />
        </feMerge>
      </filter>
      <polygon
        id="pointyBottomTriangle"
        className="shadowedTriangle pointyBottomTriangle"
        points="0,25.84618943,10.4,25.84618943,5.2,34.846189433"
        strokeOpacity={0.9}
        strokeLinecap="round"
        strokeLinejoin="bevel"
        strokeWidth={2.2}
      />
      <polygon
        id="pointyTopTriangle"
        className="shadowedTriangle pointyTopTriangle"
        points={`0,14.54618943 5.2,5.5461894 10.4,14.546189433`}
        strokeOpacity={0.9}
        strokeLinecap="round"
        strokeLinejoin="bevel"
        strokeWidth={2.2}
      />
      <polygon
        id="pointyBottomTriangleNoShadow"
        className="triangle pointyBottomTriangle"
        points="0,25.84618943,10.4,25.84618943,5.2,34.846189433"
        strokeOpacity={0.9}
        strokeLinecap="round"
        strokeLinejoin="bevel"
        strokeWidth={2.2}
      />
      <polygon
        id="pointyTopTriangleNoShadow"
        className="triangle pointyTopTriangle"
        points={`0,14.54618943 5.2,5.5461894 10.4,14.546189433`}
        strokeOpacity={0.9}
        strokeLinecap="round"
        strokeLinejoin="bevel"
        strokeWidth={2.2}
      />
      <linearGradient id="triangleGradient">
        {/* <stop offset="0%" stopColor="#AF9769"/> */}
        {/* <stop offset="50%" stopColor="#877B34"/> // #877B34 */}
        {/* <stop offset="100%" stopColor="#AF9769"/> */}
        <stop offset="0%" stopColor="#A77B24" />
        <stop offset="50%" stopColor="#877B34" />
        <stop offset="100%" stopColor="#A77B24" />
      </linearGradient>
      <linearGradient id="shadowedTriangleGradient">
        <stop offset="0%" stopColor="#a1FeF7" />
        <stop offset="50%" stopColor="#AAA" />
        {/* <stop offset="100%" stopColor="#a1De57" /> */}
      </linearGradient>
      {/* <linearGradient id="shadowedTriangleGradient">
                    <stop offset="0%" stopColor="#B78B34" />
                    <stop offset="50%" stopColor="#DDD" />
                    <stop offset="100%" stopColor="#B78B34" />
                </linearGradient> */}
      <circle id="square" cx={0} cy={0} r={squareRadius / 2} />
      <circle id="square_click" cx={0} cy={0} r={squareRadius / 1.1} /> /* same
      value as a marble to ensure we don't make mistakes predicting non pushing
      line move */
      <circle id="marbleTurnIndicator" cx={0} cy={0} r={squareRadius / 2} />
      {/* <circle id="marble" cx={0} cy={0} r={squareRadius - 0.94} /> */}
      <circle id="marble" cx={0} cy={0} r={squareRadius / 1.1} />
      <radialGradient
        id="goldenMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#E7BB94" />
        <stop offset="60%" stopColor="#B78B34" />
        <stop offset="100%" stopColor="#B78B34" />
      </radialGradient>
      <radialGradient
        id="paleGoldenMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="10%" stopColor="#D7BB94" />
        <stop offset="60%" stopColor="#bfa779" />
        <stop offset="100%" stopColor="#977B14" />

        {/* <stop offset="10%" stopColor="#D9BB94" />
          <stop offset="70%" stopColor="#bfa979" />
          <stop offset="100%" stopColor="#977B14" />         */}
      </radialGradient>
      <radialGradient
        id="rosyBrownMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="10%" stopColor="#DCAFAF" />
        <stop offset="70%" stopColor="#AC7F7F" />
        <stop offset="100%" stopColor="#6C3F3F" />
      </radialGradient>
      <radialGradient
        id="redMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="10%" stopColor="#C54A4A" />
        <stop offset="70%" stopColor="#B52A2A" />
        <stop offset="100%" stopColor="#555" />
      </radialGradient>
      <radialGradient
        id="edenMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="10%" stopColor="#5e5257" />
        <stop offset="60%" stopColor="#2e2227" />
        <stop offset="100%" stopColor="#111" />
      </radialGradient>
      {/** MARGO */}
      <radialGradient
        id="glassBlackMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#606060"></stop>
        <stop offset="25%" stopColor="#4A4A4A"></stop>
        <stop offset="50%" stopColor="#2A2A22"></stop>
        <stop offset="70%" stopColor="#1A1A11"></stop>
        <stop offset="98%" stopColor="#060600"></stop>
      </radialGradient>
      {/** MARGO */}
      <radialGradient
        id="glassWhiteMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#f4f4f4"></stop>
        <stop offset="25%" stopColor="#E8E8E8"></stop>
        <stop offset="50%" stopColor="#dfdfdf"></stop>
        <stop offset="70%" stopColor="#CCC"></stop>
        <stop offset="98%" stopColor="#888888"></stop>
      </radialGradient>
      <radialGradient
        id="funBlackMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#AAAAAF"></stop>
        <stop offset="30%" stopColor="#606066"></stop>
        <stop offset="55%" stopColor="#2A2A22"></stop>
        <stop offset="70%" stopColor="#1A1A11"></stop>
        <stop offset="98%" stopColor="#020202"></stop>

        {/* <stop offset="0%" stopColor="#A5A5A5" />
        <stop offset="25%" stopColor="#666" />
        <stop offset="50%" stopColor="#2A2A22" />
        <stop offset="70%" stopColor="#1A1A11" />
        <stop offset="98%" stopColor="#020202" /> */}

        {/* <stop offset="0%" stopColor="#888888" />
        <stop offset="25%" stopColor="#666" />
        <stop offset="50%" stopColor="#2A2A22" />
        <stop offset="70%" stopColor="#1A1A11" />
        <stop offset="98%" stopColor="#060600" /> */}
      </radialGradient>
      <radialGradient
        id="funWhiteMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#FFFFFF" />
        <stop offset="25%" stopColor="#E6E6E6" />
        <stop offset="50%" stopColor="#E6E6E6" />
        <stop offset="70%" stopColor="#dadada" />
        <stop offset="98%" stopColor="#888888" />
      </radialGradient>
      <radialGradient
        id="cosmicBlackMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#4A4A42" />
        <stop offset="35%" stopColor="#2A2A22" />
        <stop offset="98%" stopColor="#060600" />
      </radialGradient>
      <radialGradient
        id="cosmicWhiteMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        <stop offset="0%" stopColor="#FAFAFF" />
        <stop offset="25%" stopColor="#EAEAEF" />
        <stop offset="50%" stopColor="#E0E0EA" />
        <stop offset="70%" stopColor="#DADADF" />
        <stop offset="98%" stopColor="#9F9FBC" />
      </radialGradient>
      <radialGradient
        id="whiteMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        // abal.online pre 2020
        <stop offset="0%" stopColor="#FFFFFF" />
        <stop offset="60%" stopColor="#E9E9E9" />
        <stop offset="100%" stopColor="#888" />
        {/*
          <stop offset="15%" stopColor="#FFFFFA"/>
          <stop offset="80%" stopColor="#EFEDD7"/>
          <stop offset="100%" stopColor="#B6B2A7"/> */}
        // sounds good but a bit flat
        {/* <stop offset="30%" stopColor="#FFF" />
                    <stop offset="80%" stopColor="#E6E2D7" />
                    <stop offset="100%" stopColor="#C6C2B7" /> */}
        // weird crystal
        {/* <stop offset="10%" stopColor="#FFF"/>
                    <stop offset="60%" stopColor="#DDD"/>
                    <stop offset="100%" stopColor="#EEE"/>  */}
        // real flat white ones
        {/* <stop offset="15%" stopColor="#FFF"/>
                     <stop offset="80%" stopColor="#EEE"/>
                     <stop offset="100%" stopColor="#BBB"/> */}
      </radialGradient>
      <radialGradient
        id="blackMarble"
        gradientUnits="objectBoundingBox"
        fx="30%"
        fy="30%"
      >
        // black golden
        <stop offset="15%" stopColor="#6e6257" />
        <stop offset="80%" stopColor="#1e1217" />
        <stop offset="100%" stopColor="#0e0207" />
        // base reference
        {/*
                    <stop offset="0%" stopColor="#444" />
                    <stop offset="60%" stopColor="#101010" />
                    <stop offset="100%" stopColor="#000" /> */}
        // drawing
        {/* <stop offset="0%" stopColor="#555"/> */}
        {/* <stop offset="75%" stopColor="#444"/> */}
        {/* <stop offset="95%" stopColor="#222"/> */}
      </radialGradient>
      <radialGradient
        id="woodCells"
        gradientUnits="objectBoundingBox"
        r="48%"
        cx="50%"
        cy="50%"
        fx="74%"
        fy="74%"
      >
        <stop offset="0%" stopColor="#BFA779" />
        <stop offset="75%" stopColor="#877B34" />
        <stop offset="95%" stopColor="#222" />
      </radialGradient>
      <radialGradient
        id="transparentCells"
        gradientUnits="objectBoundingBox"
        r="48%"
        cx="50%"
        cy="50%"
        fx="74%"
        fy="74%"
      >
        <stop offset="0%" stopColor="#ddd" />
        <stop offset="75%" stopColor="#ddd" />
        <stop offset="95%" stopColor="#111" />
      </radialGradient>
      {/* <radialGradient
          id="transparentCells"
          gradientUnits="objectBoundingBox"
          r="48%"
          cx="50%"
          cy="50%"
          fx="74%"
          fy="74%"
        >
          <stop offset="0%" stopColor="#b258f6" />
          <stop offset="75%" stopColor="#b258f6" />
          <stop offset="95%" stopColor="#222" /> */}
      {/* <stop offset="0%" stopColor="#9238d6" />
          <stop offset="75%" stopColor="#8228c6" />
          <stop offset="95%" stopColor="#222" /> */}
      {/* </radialGradient> */}
    </defs>
  </svg>
);
