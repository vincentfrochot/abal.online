import { width } from '../../abalone.utils';
import {
  hexDist, diagonal, x, y, sin, cos,
} from '../Hexagon/hexagon.utils';

/* axes values are incrementing in this order :

to move toward the top, increment the x axis
to move toward the right, increment the y & z axes

                   /\
                  /  \
                1z    9y
                /      \
     _________ / _ _ _ _\____9x___
    \         / o o - q q\        /
     \       / o o o q q q\      /
       \    / - o o - q q -\    /
        \  / - - - - - - - -\  /
          / - - - - - - - - -\/
         /\  - - - - - - - - /\
        /  \  - q q - o o - /  \
       /    \  q q q o o o /    \
      /      \  q q - o o /      \
     /___1x__ \_ _ _ _ _ /________\
               \        /
                \      /
                1y    9z
                  \  /
                   \/

*/

export const squareRadius = diagonal / (2 * (width * 2));
const ax = x - sin;
const ay = y + cos;
const decalX = squareRadius * 2;

const getSquaresCoordinates = () => {
  const coordinates = [];

  for (let line = 0; line < 2 * width + 1; line++) {
    coordinates[line] = [];
    if (line < width + 1) {
      for (let column = 0; column < width + line + 1; column++) {
        coordinates[line][column] = {
          x: ax - line * squareRadius + column * decalX,
          y: ay - line * hexDist * squareRadius,
        };
      }
    } else {
      for (let column = 0; column < 3 * width - line + 1; column++) {
        coordinates[line][column] = {
          x: ax - (2 * width - line) * squareRadius + column * decalX,
          y: ay - line * hexDist * squareRadius,
        };
      }
    }
  }

  return coordinates;
};
export const squaresCoordinates = getSquaresCoordinates();

const getRipSquaresCoordinates = () => [
  { x: ax - squareRadius + 7 * decalX, y: ay - 1 * hexDist * squareRadius },

  { x: ax - squareRadius + 8 * decalX, y: ay - 1 * hexDist * squareRadius },

  { x: ax - squareRadius + 9 * decalX, y: ay - 1 * hexDist * squareRadius },

  { x: ax - squareRadius + 7.5 * decalX, y: ay - 2 * hexDist * squareRadius },

  { x: ax - squareRadius + 8.5 * decalX, y: ay - 2 * hexDist * squareRadius },

  { x: ax - squareRadius + 8 * decalX, y: ay - 3 * hexDist * squareRadius },

  { x: ax - squareRadius + 7 * decalX, y: ay - 9 * hexDist * squareRadius },

  { x: ax - squareRadius + 8 * decalX, y: ay - 9 * hexDist * squareRadius },

  { x: ax - squareRadius + 9 * decalX, y: ay - 9 * hexDist * squareRadius },

  { x: ax - squareRadius + 7.5 * decalX, y: ay - 8 * hexDist * squareRadius },

  { x: ax - squareRadius + 8.5 * decalX, y: ay - 8 * hexDist * squareRadius },

  { x: ax - squareRadius + 8 * decalX, y: ay - 7 * hexDist * squareRadius },
];
export const ripSquaresCoordinates = getRipSquaresCoordinates();

// coordinates 2D is used for square and marbles position
export const coordinates2D = [
  [1, 1],
  [1, 2],
  [1, 3],
  [1, 4],
  [1, 5],

  [2, 1],
  [2, 2],
  [2, 3],
  [2, 4],
  [2, 5],
  [2, 6],

  [3, 1],
  [3, 2],
  [3, 3],
  [3, 4],
  [3, 5],
  [3, 6],
  [3, 7],

  [4, 1],
  [4, 2],
  [4, 3],
  [4, 4],
  [4, 5],
  [4, 6],
  [4, 7],
  [4, 8],

  [5, 1],
  [5, 2],
  [5, 3],
  [5, 4],
  [5, 5],
  [5, 6],
  [5, 7],
  [5, 8],
  [5, 9],

  [6, 1],
  [6, 2],
  [6, 3],
  [6, 4],
  [6, 5],
  [6, 6],
  [6, 7],
  [6, 8],

  [7, 1],
  [7, 2],
  [7, 3],
  [7, 4],
  [7, 5],
  [7, 6],
  [7, 7],

  [8, 1],
  [8, 2],
  [8, 3],
  [8, 4],
  [8, 5],
  [8, 6],

  [9, 1],
  [9, 2],
  [9, 3],
  [9, 4],
  [9, 5],
];

// coordinates3D is used for moves : : if value changed for the 3 axes, player tried a side move (making it easy to forbid this for clash)
export const coordinates3D = [
  // 3 axes are : x:- y:\ z:/
  [1, 1, 5],
  [1, 2, 6],
  [1, 3, 7],
  [1, 4, 8],
  [1, 5, 9],

  [2, 1, 4],
  [2, 2, 5],
  [2, 3, 6],
  [2, 4, 7],
  [2, 5, 8],
  [2, 6, 9],

  [3, 1, 3],
  [3, 2, 4],
  [3, 3, 5],
  [3, 4, 6],
  [3, 5, 7],
  [3, 6, 8],
  [3, 7, 9],

  [4, 1, 2],
  [4, 2, 3],
  [4, 3, 4],
  [4, 4, 5],
  [4, 5, 6],
  [4, 6, 7],
  [4, 7, 8],
  [4, 8, 9],

  [5, 1, 1],
  [5, 2, 2],
  [5, 3, 3],
  [5, 4, 4],
  [5, 5, 5],
  [5, 6, 6],
  [5, 7, 7],
  [5, 8, 8],
  [5, 9, 9],

  [6, 2, 1],
  [6, 3, 2],
  [6, 4, 3],
  [6, 5, 4],
  [6, 6, 5],
  [6, 7, 6],
  [6, 8, 7],
  [6, 9, 8],

  [7, 3, 1],
  [7, 4, 2],
  [7, 5, 3],
  [7, 6, 4],
  [7, 7, 5],
  [7, 8, 6],
  [7, 9, 7],

  [8, 4, 1],
  [8, 5, 2],
  [8, 6, 3],
  [8, 7, 4],
  [8, 8, 5],
  [8, 9, 6],

  [9, 5, 1],
  [9, 6, 2],
  [9, 7, 3],
  [9, 8, 4],
  [9, 9, 5],
];

export const coordinates3DTo1D = [
  '115',
  '126',
  '137',
  '148',
  '159',

  '214',
  '225',
  '236',
  '247',
  '258',
  '269',

  '313',
  '324',
  '335',
  '346',
  '357',
  '368',
  '379',

  '412',
  '423',
  '434',
  '445',
  '456',
  '467',
  '478',
  '489',

  '511',
  '522',
  '533',
  '544',
  '555',
  '566',
  '577',
  '588',
  '599',

  '621',
  '632',
  '643',
  '654',
  '665',
  '676',
  '687',
  '698',

  '731',
  '742',
  '753',
  '764',
  '775',
  '786',
  '797',

  '841',
  '852',
  '863',
  '874',
  '885',
  '896',

  '951',
  '962',
  '973',
  '984',
  '995',
];
