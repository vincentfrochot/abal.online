import React, { useState } from 'react';

import './square.scss';

export const Square = (props) => {
  const {
    ripSquare, x, y, position, clickHandler,
  } = props;

  const [selected, setSelected] = useState(false);

  const handleClick = () => {
    setSelected((selected) => !selected);
    clickHandler(position);
  };

  return (
    <>
      {ripSquare ? (
        <>
          <use
            xlinkHref="#square"
            x={x}
            y={y}
            fillOpacity={0.65}
            className="square"
            key={`ripsquare-${x}-${y}`}
          />
          <use
            xlinkHref="#square_click"
            x={x}
            y={y}
            fill="transparent"
            strokeWidth="0.5"
            key={`ripsquare-clickable-${x}-${y}`}
          />
        </>
      ) : (
        <>
          <use
            xlinkHref="#square"
            x={x}
            y={y}
            fillOpacity={0.65}
            className="square"
            key={`square-${position}`}
          />
          <use
            xlinkHref="#square_click"
            x={x}
            y={y}
            fill="transparent"
            strokeWidth="0.5"
            onClick={handleClick} // on peut mettre un memo sur le handleClick si on le branche comme ça. Ici c'est un pointeur sur la fonction au lieu de créer une nouvelle fonction à chaque render
            key={`square-clickable-${position}`}
          />
        </>
      )}
    </>
  );
};
