import React from 'react';
import { Arrow } from '../Arrow/Arrow';

export const Marble = ({
  position, player, x, y, rip, selected, moved, color, opponentColor, clickHandler,
}) => {
  const handleClick = () => {
    clickHandler(position); // dans l'idéal c'est la position qui détermine le rerender via react.memo, à placer dans le component parent.
  };
  // @TODO: check comment utiliser le hook useMemo(). A utiliser pour memoizer un résultat dépendant de paramètres

  return (
    <>
      <use
        href={position === 666 ? '#marbleTurnIndicator' : '#marble'}
        className={`marble ${color}`}
        x={x}
        y={y}
        fillOpacity={1}
        stroke={player == 0 ? 'white' : '#020202'}
        strokeOpacity={1}
        strokeWidth={selected ? 1.5 : 0}
        onClick={handleClick} // pointeur sur fonction => donc on peut utiliser react.memo
      />
      {moved && (
        <Arrow
          x={x}
          y={y}
          className={`arrow ${opponentColor}`}
          direction={moved}
          radius={13}
        />
      )}
    </>
  );
};
