import './gameCard.scss';

import React, { useState, useEffect, useContext, useRef } from 'react';
import {
  Grid,
  Paper,
  Button,
  Snackbar,
  IconButton,
  Select,
  InputLabel,
} from '@material-ui/core';
import { Game } from '../Game/Game';
import { viewBoxPredator, viewBoxGuardian } from '../Hexagon/hexagon.utils';
import {
  SocketContext,
  SOCKET_EVENTS,
} from '../../../../contexts/SocketContext';
import { SocketGame } from '../SocketGame/SocketGame';
import GamesContext from '../../../../contexts/GamesContext';
import TemplateContext from '../../../../contexts/TemplateContext';
import UserContext from '../../../../contexts/UserContext';
import { FormattedMessage } from 'react-intl';

export const GameCard = (props) => {
  const {
    // challengers,
    // gameStarted,
    // histoPositions,
    // lastMoveNumber
    quitGameHandler,
    multipleGames,
    sidentifier,
    // stype,
    // svariant,
    // sstartClockP1,
    // sstartClockP2,
    // sowner,
    // schallengers,
    // splayers,
    // swatchers,
    // sstart,
    // sfinished,
    // slastMoveIndex,
    // shistoTimes,
    // shistoScores,
    // shistoVectors,
    // shistoPositions,
  } = props;

  const userContext = useContext(UserContext);
  const socketContext = useContext(SocketContext);
  const { gamesContext } = useContext(GamesContext);
  const { templateGamesColContext } = useContext(TemplateContext);

  const game = gamesContext.games.find((e) => e.id === sidentifier);
  if (!game) {
    quitGameHandler(sidentifier);
  }

  const [moveIndex, setMoveIndex] = useState(game.lastMoveIndex);
  // const [gameIsStarted, setGameIsStarted] = useState(false);
  const [gameIsFinished, setGameIsFinished] = useState(false);
  const [notifyGameIsFinished, setNotifyGameIsFinished] = useState(false);
  const [opponent, setOpponent] = useState('');

  const amIBlack =
    game.players[0] && game.players[0] === userContext.userContext.nickname;
  const amIWhite =
    game.players[1] && game.players[1] === userContext.userContext.nickname;
  const blackTurn = game.lastMoveIndex % 2 === 0;
  const shouldIPlay =
    ((blackTurn && amIBlack) || (!blackTurn && amIWhite)) && !game.finished;
  const className =
    shouldIPlay && game.type === 'match' ? 'match_your_turn' : '';

  useEffect(() => {
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_challenged_owner,
      handleNewChallenger,
    );
    socketContext.socket.on(
      SOCKET_EVENTS.server.game.game_canceled_challenge_owner,
      handleRemoveChallenger,
    );

    return function cleanup() {
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_challenged_owner,
        handleNewChallenger,
      );
      socketContext.socket.removeListener(
        SOCKET_EVENTS.server.game.game_canceled_challenge_owner,
        handleRemoveChallenger,
      );
    };
  }, []);

  const handleNewChallenger = (data) => {
    if (data.content) {
      const gameId = data.content.gameId;
      const newChallengerNickname = data.content.newChallenger;
      if (sidentifier === gameId) {
        setOpponent((previous) => {
          if (previous === '') {
            return newChallengerNickname;
          }
          return previous;
        });
      }
    }
  };

  const handleRemoveChallenger = (data) => {
    if (data.content) {
      const gameId = data.content.gameId;
      if (sidentifier === gameId) {
        setOpponent('');
      }
    }
  };

  const handleChooseChallenger = (e) => {
    const opponentNickname = e.target.value;
    setOpponent(opponentNickname);
  };

  const quitGame = () => {
    quitGameHandler(sidentifier);
  };

  const handleUndo = () => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_move_undo, {
      gameId: sidentifier,
    });
  };

  const startGame = () => {
    // setGameIsStarted(true);
    // @TODO: send EVENT
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_start, {
      id: sidentifier,
      nickname: opponent,
    });
  };

  const finishGame = () => {
    setGameIsFinished(true);
    setNotifyGameIsFinished(true);
  };

  const moveBackward = () => {
    setMoveIndex((prev) => {
      if (!prev) {
        return 0;
      }
      return prev - 1;
    });
  };

  const moveForward = () => {
    setMoveIndex((prev) => prev + 1);
  };

  const moveToFirst = () => {
    setMoveIndex(0);
  };

  const moveToLast = () => {
    setMoveIndex(game.lastMoveIndex);
  };

  const closeSnackBar = () => {
    setNotifyGameIsFinished(false);
  };

  const createAnalysis = () => {
    socketContext.socket.emit(SOCKET_EVENTS.client.game.game_analyze, {
      gameId: sidentifier,
    });
  };

  return (
    <Grid
      item
      xs={multipleGames ? parseInt(templateGamesColContext) : 12}
      key={`game-${sidentifier}`}
      style={{ margin: '0px', padding: multipleGames ? '3px' : '5px' }}
    >
      <Paper
        className={'gamecard-paper ' + className}
        style={{
          margin: '0px',
          padding: '0px',
          backgroundColor: '#ddd',
        }}
      >
        {/* <Button>Play</Button>
        <Button>Watch</Button> */}
        {!game.started && (
          <>
            <InputLabel htmlFor="opponent-select">
              <FormattedMessage id="app.page.home.game--choose-opponent" />
            </InputLabel>
            <Select
              native
              value={opponent}
              onChange={(e) => handleChooseChallenger(e)}
              inputProps={{
                name: 'opponent',
                id: 'opponent-select',
              }}
            >
              <option aria-label="None" value="" />
              {game.challengers.map((element, index) => (
                <option key={'nickname' + index} value={element}>
                  {element}
                </option>
              ))}
            </Select>
            <Button
              className="gamecard-paper__start-game-button"
              onClick={startGame}
            >
              <FormattedMessage id="app.page.home.game--start-button" />
            </Button>
          </>
        )}
        <Button className="rightAlign" onClick={quitGame}>
          <FormattedMessage id="app.page.home.game--quit-button" />
        </Button>
        {game.started && game.type}
        <br />
        {game.watchers.length} people watching
        <br />
        {game.started && (
          <Button onClick={createAnalysis}>
            <FormattedMessage id="app.page.home.game--analyse-button" />
          </Button>
        )}
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={notifyGameIsFinished}
          autoHideDuration={6000}
          // onClose={handleClose}
          message={game.winner + ' won.'}
          action={
            <>
              <Button color="secondary" size="small" onClick={closeSnackBar}>
                OK
              </Button>
              <IconButton size="small" aria-label="close" color="inherit" />
            </>
          }
        />
        <svg
          viewBox={multipleGames ? viewBoxGuardian : viewBoxPredator}
          preserveAspectRatio="xMinYMin meet"
        >
          <SocketGame
            key={`game-${sidentifier}-${game.lastMoveIndex}`}
            displayedMoveIndex={moveIndex}
            moveCallBack={setMoveIndex}
            endgameHandler={finishGame}
            sidentifier={sidentifier}
            // svariant={svariant}
            // sstartClockP1={sstartClockP1}
            // sstartClockP2={sstartClockP2}
            // sowner={sowner}
            // sstarted,
            // sfinished,
            // shistoryTimes,
            // shistoryScores,
            // shistoryVectors,
            // shistoryPositions,
            // smyColor, // <=> do we send the move we will make. Be careful about the check on how and when to send moves.
            // moveToDisplay, // set with prev next
          />
          {/* <Game
            identifier={sidentifier}
            key={finishGame}
            viewBox={multipleGames ? viewBoxGuardian : viewBoxPredator}
            move={move}
            moveCallBack={setMove}
            endgameHandler={finishGame}
            started={gameIsStarted}
            finished={gameIsFinished}
            variant={variant}
            scoreP1={scoreP1}
            scoreP2={scoreP2}
            startClockP1={startClockP1}
            startClockP2={startClockP2}
            histoPositions={histoPositions}
            lastMoveNumber={lastMoveNumber}
          /> */}
        </svg>
        {game.finished || game.type === 'analysis' ? (
          <>
            {moveIndex}/{game.lastMoveIndex}<br />
            {/* {game.histoPositions[moveIndex].join(',')} <br /> */}
            {/* <br /> */}
          </>
        ) : (
          <>
            <FormattedMessage id="app.page.home.game--move" /> #{moveIndex}
          </>
        )}
        {(game.finished || game.type === 'analysis') && (
          <Button onClick={moveToFirst}>
            <FormattedMessage id="app.page.home.game--first-button" />
          </Button>
        )}
        {(game.finished || game.type === 'analysis') && (
          <Button onClick={moveBackward}>
            <FormattedMessage id="app.page.home.game--prev-button" />
          </Button>
        )}
        {(game.finished || game.type === 'analysis') && (
          <Button onClick={moveForward}>
            <FormattedMessage id="app.page.home.game--next-button" />
          </Button>
        )}
        {(game.finished || game.type === 'analysis') && (
          <Button onClick={moveToLast}>
            <FormattedMessage id="app.page.home.game--last-button" />
          </Button>
        )}
        {game.type === 'analysis' && (
          <Button onClick={handleUndo}>
            <FormattedMessage id="app.page.home.game--undo-button" />
          </Button>
        )}
      </Paper>
    </Grid>
  );
};
