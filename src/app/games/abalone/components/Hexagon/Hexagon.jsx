import React, { useContext } from 'react';
import { hexagonPointsList } from './hexagon.utils';
import AbaloneThemeContext from '../../contexts/AbaloneThemeContext';

export const Hexagon = (props) => {
  const {
    gametype = match,
    finished = false,
  } = props;
  const boardContext = useContext(AbaloneThemeContext);

  const className = (gametype === "analysis" || finished) ? "glass" : boardContext.board.color;
  return (
    <polygon
      className={`${className} hexagon`}
      strokeOpacity="1"
      strokeLinecap="round"
      strokeLinejoin="bevel"
      strokeWidth={2}
      points={hexagonPointsList}
    />
  );
};
