import { width } from '../../abalone.utils';

// export const viewBox = `-40 0 ${650} ${320}`;
export const viewBoxGuardian = '-40 -10 400 310';
export const viewBoxPredator = '-100 0 500 320';

export const diagonal = 290;
export const hexDist = 1.73; // hexagon height is equal to hexDist * size of 1 side
export const y = diagonal / 2;
const radius = y;
const hexRatio = 0.868217054;
const cx = (hexRatio * diagonal) / 2;
const round = (num) => Number(num.toFixed(3));
export const x = cx + (y - cx) + 2 * width;
export const sin = 0.5 * radius;
export const cos = 0.86602540378 * radius;

const hexagonPoints = [
  [x - sin, y - cos],
  [x + sin, y - cos],
  [x + radius, y],
  [x + sin, y + cos],
  [x - sin, y + cos],
  [x - radius, y],
].map((point) => point.map(round));

export const hexagonPointsList = hexagonPoints
  .map((point) => point.join(','))
  .join(' ');
