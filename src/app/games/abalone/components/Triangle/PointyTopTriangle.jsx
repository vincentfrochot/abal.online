import React from 'react';
import { squaresCoordinates } from '../Square/square.utils';

export const PointyTopTriangle = ({ position2D, hdr = true }) => (
  <use
    xlinkHref={hdr ? "#pointyTopTriangle" : "#pointyTopTriangleNoShadow"}
    x={squaresCoordinates[position2D[0]][position2D[1]].x - 19.5}
    y={squaresCoordinates[position2D[0]][position2D[1]].y - 19.5}
  />
);
