import React from 'react';
import { Square } from '../Square/Square';
import {
  coordinates2D,
  ripSquaresCoordinates,
  squaresCoordinates,
} from '../Square/square.utils';

export const SquareList = React.memo(
  ({ clickHandler }) => {
    const squares = coordinates2D.map((position2D, position1D) => (
      <Square
        key={`square-${position1D}`}
        x={
            squaresCoordinates[coordinates2D[position1D][0]][
              coordinates2D[position1D][1]
            ].x
          }
        y={
            squaresCoordinates[coordinates2D[position1D][0]][
              coordinates2D[position1D][1]
            ].y
          }
        position={position1D}
        ripSquare={false}
        clickHandler={clickHandler}
      />
    ));
    
    const ripSquares = ripSquaresCoordinates.map((position2D, position1D) => {
      const { x } = ripSquaresCoordinates[position1D];
      const { y } = ripSquaresCoordinates[position1D];

      return (
        <Square
          key={`ripsquare-x-${x}-y-${y}`}
          x={x}
          y={y}
          position={position1D}
          ripSquare
        />
      );
    });

    return (
      <>
        {squares}
        {' '}
        {ripSquares}
      </>
    );
  },
  () => true,
);
