import React from 'react';

const AbaloneSnapshotContext = React.createContext({
  height: 290,
  width: 349,
  scale: 2,
  backGroundColor: 'transparent',
});

export default AbaloneSnapshotContext;
