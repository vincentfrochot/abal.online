import React from 'react';

const AbaloneThemeContext = React.createContext({
  board: {
    color: 'fun',
    displayCoordinates: true,
  },
  marbles: {
    color: ['fun0', 'fun1'],
  },
});

export default AbaloneThemeContext;
